ALTER TABLE uporabnik
ADD FOREIGN KEY (id_tip_uporabnik)
REFERENCES tip_uporabnik(id_tip_uporabnik);

ALTER TABLE uporabnik
ADD FOREIGN KEY (id_lokacija)
REFERENCES lokacija(id_lokacija);

ALTER TABLE stanovanje
ADD FOREIGN KEY (id_lokacija)
REFERENCES lokacija(id_lokacija);

ALTER TABLE stanovanje
ADD FOREIGN KEY (id_tip_stanovanje)
REFERENCES tip_stanovanje(id_tip_stanovanje);

ALTER TABLE uporabnik_stanovanje
ADD FOREIGN KEY (id_uporabnik)
REFERENCES uporabnik(id_uporabnik);

ALTER TABLE uporabnik_stanovanje
ADD FOREIGN KEY (id_stanovanje)
REFERENCES stanovanje(id_stanovanje);

ALTER TABLE stanovanje_kriterij
ADD FOREIGN KEY (id_stanovanje)
REFERENCES stanovanje(id_stanovanje);

ALTER TABLE stanovanje_kriterij
ADD FOREIGN KEY (id_kriterij)
REFERENCES kriterij(id_kriterij);

ALTER TABLE user_kriterij
ADD FOREIGN KEY (id_uporabnik)
REFERENCES uporabnik(id_uporabnik);

ALTER TABLE user_kriterij
ADD FOREIGN KEY (id_kriterij)
REFERENCES kriterij(id_kriterij);



