CREATE TABLE IF NOT EXISTS `tip_uporabnik` (
  `id_tip_uporabnik` INT NOT NULL,
  `naziv` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_tip_uporabnik`));
  
  CREATE TABLE IF NOT EXISTS `cimerkisi`.`tip_stanovanje` (
  `id_tip_stanovanje` INT NOT NULL,
  `naziv` VARCHAR(45) NOT NULL,
  `st_sob_oddaja` INT NOT NULL,
  `celotno` TINYINT(1) NOT NULL,
  PRIMARY KEY (`id_tip_stanovanje`));
  
  CREATE TABLE IF NOT EXISTS `cimerkisi`.`kriterij` (
  `id_kriterij` INT NOT NULL,
  `kadilec` TINYINT(1) NULL,
  `prenasanje_kadilca` INT NULL,
  `cistoca` INT NULL,
  `prenasanje_necistoce` INT NULL,
  `lastnik_zivali` TINYINT(1) NULL,
  `ljubitelj_zivali` INT NULL COMMENT '0-5 -> stopnje',
  `glasnost_glasbe` INT NULL COMMENT '0 -> slušalke\n1-5 -> stopnje',
  `osebnost` INT NULL COMMENT '1-5 -> stopnje\n(stroga; na izi)',
  `pref_spol` CHAR(1) NULL,
  `hobi_1` VARCHAR(45) NULL,
  `hobi_2` VARCHAR(45) NULL,
  `hobi_3` VARCHAR(45) NULL,
  `avto_moto` TINYINT(1) NULL,
  `invalid` TINYINT(1) NULL,
  `stan_parking` TINYINT(1) NULL,
  `stan_dvigalo` TINYINT(1) NULL,
  `stan_balkon` TINYINT(1) NULL,
  `stan_trgovina` TINYINT(1) NULL,
  `stan_center_mesta` TINYINT(1) NULL,
  `stan_center_mesta_razdalja` INT NULL,
  `stan_pralni_stroj` TINYINT(1) NULL,
  `stan_pecica` TINYINT(1) NULL,
  `stan_stedilnik` TINYINT(1) NULL,
  `stan_zivali` TINYINT(1) NULL,
  `stan_kadilci` TINYINT(1) NULL,
  `stan_sosedi` INT NULL COMMENT '1-5 -> stopnje\n(slabi; dobri)',
  `stan_kolesarnica` VARCHAR(45) NULL,
  `stan_obisk_dovoljen` TINYINT(1) NULL,
  `stan_najemodajalec_obisk` INT NULL COMMENT '1-5 -> stopnje\n(redko; pogosto)',
  PRIMARY KEY (`id_kriterij`));
  
  CREATE TABLE IF NOT EXISTS `cimerkisi`.`lokacija` (
  `id_lokacija` INT NOT NULL,
  `mesto` VARCHAR(45) NOT NULL,
  `postna_st` INT NOT NULL,
  `naslov` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_lokacija`));
  
  CREATE TABLE IF NOT EXISTS `cimerkisi`.`uporabnik` (
  `id_uporabnik` INT NOT NULL,
  `id_tip_uporabnik` INT NOT NULL,
  `id_lokacija` INT NOT NULL,
  `ime` VARCHAR(45) NOT NULL,
  `priimek` VARCHAR(45) NOT NULL,
  `mail` VARCHAR(45) NOT NULL,
  `tel_st` INT NOT NULL,
  `geslo` VARCHAR(45) NOT NULL,
  `status` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_uporabnik`));
  
  CREATE TABLE IF NOT EXISTS `cimerkisi`.`stanovanje` (
  `id_stanovanje` INT NOT NULL,
  `id_lokacija` INT NOT NULL,
  `id_tip_stanovanje` INT NOT NULL,
  `stroski_fiksni` TINYINT(1) NOT NULL,
  `stroski_vrednost` DOUBLE NOT NULL,
  `najemnina_vrednost` DOUBLE NOT NULL,
  PRIMARY KEY (`id_stanovanje`));
  
  CREATE TABLE IF NOT EXISTS `cimerkisi`.`user_search` (
  `id_user_search` INT NOT NULL,
  PRIMARY KEY (`id_user_search`));
  
  CREATE TABLE IF NOT EXISTS `cimerkisi`.`uporabnik_stanovanje` (
  `id_uporabnik_stanovanje` INT NOT NULL,
  `id_uporabnik` INT NOT NULL,
  `id_stanovanje` INT NOT NULL,
  PRIMARY KEY (`id_uporabnik_stanovanje`));
  
  CREATE TABLE IF NOT EXISTS `cimerkisi`.`user_kriterij` (
  `id_uporabnik_kriterij` INT NOT NULL,
  `id_uporabnik` INT NOT NULL,
  `id_kriterij` INT NOT NULL,
  PRIMARY KEY (`id_uporabnik_kriterij`));
  
  CREATE TABLE IF NOT EXISTS `cimerkisi`.`stanovanje_kriterij` (
  `id_stanovanje_kriterij` INT NOT NULL,
  `id_stanovanje` INT NOT NULL,
  `id_kriterij` INT NOT NULL,
  PRIMARY KEY (`id_stanovanje_kriterij`));
  
  

  