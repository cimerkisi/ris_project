/*ročno vnešeni testni podatki*/

/*-----------------------------------------------------------*/

INSERT INTO `cimerkisi`.`tip_uporabnik` (`id_tip_uporabnik`, `naziv`) VALUES (1, 'iskalec_stanovanja');
INSERT INTO `cimerkisi`.`tip_uporabnik` (`id_tip_uporabnik`, `naziv`) VALUES (2, 'imetnik_stanovanja');
INSERT INTO `cimerkisi`.`tip_uporabnik` (`id_tip_uporabnik`, `naziv`) VALUES (3, 'iskalec_cimra');

/*-----------------------------------------------------------*/

INSERT INTO `cimerkisi`.`tip_stanovanje` (`id_tip_stanovanje`, `naziv`) VALUES (1, 'hisa');
INSERT INTO `cimerkisi`.`tip_stanovanje` (`id_tip_stanovanje`, `naziv`) VALUES (2, 'blok');
INSERT INTO `cimerkisi`.`tip_stanovanje` (`id_tip_stanovanje`, `naziv`) VALUES (3, 'vrstna hisa');
INSERT INTO `cimerkisi`.`tip_stanovanje` (`id_tip_stanovanje`, `naziv`) VALUES (4, 'vecstanovanjska hisa');
INSERT INTO `cimerkisi`.`tip_stanovanje` (`id_tip_stanovanje`, `naziv`) VALUES (5, 'drevo');

/*-----------------------------------------------------------*/

INSERT INTO `cimerkisi`.`kriterij` (`id_kriterij`, `kadilec`, `prenasanje_kadilca`, `cistoca`, `prenasanje_necistoce`, `lastnik_zivali`, `ljubitelj_zivali`, `glasnost_glasbe`, `osebnost`, `pref_spol`, `hobi_1`, `hobi_2`, `hobi_3`, `avto_moto`, `invalid`, `stan_parking`, `stan_dvigalo`, `stan_balkon`, `stan_trgovina`, `stan_center_mesta`, `stan_center_mesta_razdalja`, `stan_pralni_stroj`, `stan_pecica`, `stan_stedilnik`, `stan_zivali`, `stan_kadilci`, `stan_sosedi`, `stan_kolesarnica`, `stan_obisk_dovoljen`, `stan_najemodajalec_obisk`) VALUES (1, 1, 1, 1, 1, 1, 1, 1, 1, 'm', 'nogomet', 'judo', 'matematika', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `cimerkisi`.`kriterij` (`id_kriterij`, `kadilec`, `prenasanje_kadilca`, `cistoca`, `prenasanje_necistoce`, `lastnik_zivali`, `ljubitelj_zivali`, `glasnost_glasbe`, `osebnost`, `pref_spol`, `hobi_1`, `hobi_2`, `hobi_3`, `avto_moto`, `invalid`, `stan_parking`, `stan_dvigalo`, `stan_balkon`, `stan_trgovina`, `stan_center_mesta`, `stan_center_mesta_razdalja`, `stan_pralni_stroj`, `stan_pecica`, `stan_stedilnik`, `stan_zivali`, `stan_kadilci`, `stan_sosedi`, `stan_kolesarnica`, `stan_obisk_dovoljen`, `stan_najemodajalec_obisk`) VALUES (2, 0, 0, 0, 0, 0, 0, 0, 0, 'z', 'odbojka', 'ucenje jezikov', 'varjenje', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `cimerkisi`.`kriterij` (`id_kriterij`, `kadilec`, `prenasanje_kadilca`, `cistoca`, `prenasanje_necistoce`, `lastnik_zivali`, `ljubitelj_zivali`, `glasnost_glasbe`, `osebnost`, `pref_spol`, `hobi_1`, `hobi_2`, `hobi_3`, `avto_moto`, `invalid`, `stan_parking`, `stan_dvigalo`, `stan_balkon`, `stan_trgovina`, `stan_center_mesta`, `stan_center_mesta_razdalja`, `stan_pralni_stroj`, `stan_pecica`, `stan_stedilnik`, `stan_zivali`, `stan_kadilci`, `stan_sosedi`, `stan_kolesarnica`, `stan_obisk_dovoljen`, `stan_najemodajalec_obisk`) VALUES (3, 1, 3, 4, 5, 1, 4, 3, 2, 'm', 'jahanje', 'šah', 'programiranje', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `cimerkisi`.`kriterij` (`id_kriterij`, `kadilec`, `prenasanje_kadilca`, `cistoca`, `prenasanje_necistoce`, `lastnik_zivali`, `ljubitelj_zivali`, `glasnost_glasbe`, `osebnost`, `pref_spol`, `hobi_1`, `hobi_2`, `hobi_3`, `avto_moto`, `invalid`, `stan_parking`, `stan_dvigalo`, `stan_balkon`, `stan_trgovina`, `stan_center_mesta`, `stan_center_mesta_razdalja`, `stan_pralni_stroj`, `stan_pecica`, `stan_stedilnik`, `stan_zivali`, `stan_kadilci`, `stan_sosedi`, `stan_kolesarnica`, `stan_obisk_dovoljen`, `stan_najemodajalec_obisk`) VALUES (4, 0, 5, 4, 3, 0, 2, 2, 4, 'z', 'sivanje', 'burek', 'jadranje', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `cimerkisi`.`kriterij` (`id_kriterij`, `kadilec`, `prenasanje_kadilca`, `cistoca`, `prenasanje_necistoce`, `lastnik_zivali`, `ljubitelj_zivali`, `glasnost_glasbe`, `osebnost`, `pref_spol`, `hobi_1`, `hobi_2`, `hobi_3`, `avto_moto`, `invalid`, `stan_parking`, `stan_dvigalo`, `stan_balkon`, `stan_trgovina`, `stan_center_mesta`, `stan_center_mesta_razdalja`, `stan_pralni_stroj`, `stan_pecica`, `stan_stedilnik`, `stan_zivali`, `stan_kadilci`, `stan_sosedi`, `stan_kolesarnica`, `stan_obisk_dovoljen`, `stan_najemodajalec_obisk`) VALUES (5, 1, 4, 4, 4, 1, 1, 2, 3, 'm', 'spanje', 'igranje iger', 'sprehajanje', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

/*-----------------------------------------------------------*/

INSERT INTO `cimerkisi`.`lokacija` (`id_lokacija`, `mesto`, `postna_st`, `naslov`) VALUES (1, 'maribor', 2000, 'gosposvetska cesta 30');
INSERT INTO `cimerkisi`.`lokacija` (`id_lokacija`, `mesto`, `postna_st`, `naslov`) VALUES (2, 'celje', 3000, 'stritarjeva ulica 7');
INSERT INTO `cimerkisi`.`lokacija` (`id_lokacija`, `mesto`, `postna_st`, `naslov`) VALUES (3, 'ljubljana', 1000, 'dunajska cesta 1');
INSERT INTO `cimerkisi`.`lokacija` (`id_lokacija`, `mesto`, `postna_st`, `naslov`) VALUES (4, 'kranj', 4000, 'cankarjeva ulica 4');
INSERT INTO `cimerkisi`.`lokacija` (`id_lokacija`, `mesto`, `postna_st`, `naslov`) VALUES (5, 'koper', 6000, 'hmeljarska pot 67');
INSERT INTO `cimerkisi`.`lokacija` (`id_lokacija`, `mesto`, `postna_st`, `naslov`) VALUES (6, 'maribor', 2000, 'strossmayerjeva ulica 53');
INSERT INTO `cimerkisi`.`lokacija` (`id_lokacija`, `mesto`, `postna_st`, `naslov`) VALUES (7, 'celje', 3000, 'kersnikova ulica 32');
INSERT INTO `cimerkisi`.`lokacija` (`id_lokacija`, `mesto`, `postna_st`, `naslov`) VALUES (8, 'maribor', 2000, 'smetanova ulica 17');
INSERT INTO `cimerkisi`.`lokacija` (`id_lokacija`, `mesto`, `postna_st`, `naslov`) VALUES (9, 'celje', 3000, 'nusiceva ulica 2a');
INSERT INTO `cimerkisi`.`lokacija` (`id_lokacija`, `mesto`, `postna_st`, `naslov`) VALUES (10, 'ljubljana', 1000, 'celjska cesta 45');

/*-----------------------------------------------------------*/


INSERT INTO `cimerkisi`.`uporabnik` (`id_uporabnik`, `id_tip_uporabnik`, `id_lokacija`, `ime`, `priimek`, `mail`, `tel_st`, `geslo`, `status`) VALUES (1, 1, 1, 'janez', 'novak', 'janez.novak@mail.si', 123456789, '1', 'student');
INSERT INTO `cimerkisi`.`uporabnik` (`id_uporabnik`, `id_tip_uporabnik`, `id_lokacija`, `ime`, `priimek`, `mail`, `tel_st`, `geslo`, `status`) VALUES (2, 2, 2, 'miha', 'strel', 'miha.strel@mail.ci', 123456789, '1', 'zaposlen');
INSERT INTO `cimerkisi`.`uporabnik` (`id_uporabnik`, `id_tip_uporabnik`, `id_lokacija`, `ime`, `priimek`, `mail`, `tel_st`, `geslo`, `status`) VALUES (3, 3, 3, 'stefka', 'zdolsek', 'stefka.zdolsek@telemach.com', 123456789, '1', 'nezaposlen');
INSERT INTO `cimerkisi`.`uporabnik` (`id_uporabnik`, `id_tip_uporabnik`, `id_lokacija`, `ime`, `priimek`, `mail`, `tel_st`, `geslo`, `status`) VALUES (4, 1, 4, 'gordana', 'veselinovic', 'gordana veselinovic@burek.si', 123456789, '1', 'student');
INSERT INTO `cimerkisi`.`uporabnik` (`id_uporabnik`, `id_tip_uporabnik`, `id_lokacija`, `ime`, `priimek`, `mail`, `tel_st`, `geslo`, `status`) VALUES (5, 2, 5, 'milojka', 'drnovsek', 'milojka.drnovsek@gmail.vom', 123456789, '1', 'nezaposlen');
INSERT INTO `cimerkisi`.`uporabnik` (`id_uporabnik`, `id_tip_uporabnik`, `id_lokacija`, `ime`, `priimek`, `mail`, `tel_st`, `geslo`, `status`) VALUES (6, 3, 6, 'smiljan', 'merkel', 'merkel.smiljan@siol.net', 123456789, '1', 'zaposlen');
INSERT INTO `cimerkisi`.`uporabnik` (`id_uporabnik`, `id_tip_uporabnik`, `id_lokacija`, `ime`, `priimek`, `mail`, `tel_st`, `geslo`, `status`) VALUES (7, 1, 7, 'karel', 'erjavec', 'karel.erjavec@ess.gov.si', 123456789, '1', 'student');
INSERT INTO `cimerkisi`.`uporabnik` (`id_uporabnik`, `id_tip_uporabnik`, `id_lokacija`, `ime`, `priimek`, `mail`, `tel_st`, `geslo`, `status`) VALUES (8, 2, 8, 'nik', 'kapun', 'nik.kapun@olimpija.si', 123456789, '1', 'student');
INSERT INTO `cimerkisi`.`uporabnik` (`id_uporabnik`, `id_tip_uporabnik`, `id_lokacija`, `ime`, `priimek`, `mail`, `tel_st`, `geslo`, `status`) VALUES (9, 3, 9, 'matjaz', 'zupan', 'matjaz.z@zmail.si', 123456789, '1', 'student');
INSERT INTO `cimerkisi`.`uporabnik` (`id_uporabnik`, `id_tip_uporabnik`, `id_lokacija`, `ime`, `priimek`, `mail`, `tel_st`, `geslo`, `status`) VALUES (10, 3, 10, 'jernej', 'muha', 'jerryjecar@gmail.com', 123456789, '1', 'student');

/*-----------------------------------------------------------*/

INSERT INTO `cimerkisi`.`stanovanje` (`id_stanovanje`, `id_lokacija`, `id_tip_stanovanje`, `stroski_fiksni`, `stroski_vrednost`, `najemnina_vrednost`, `stevilo_sob`) VALUES (1, 2, 1, 0, 23.5, 150, 3);
INSERT INTO `cimerkisi`.`stanovanje` (`id_stanovanje`, `id_lokacija`, `id_tip_stanovanje`, `stroski_fiksni`, `stroski_vrednost`, `najemnina_vrednost`, `stevilo_sob`) VALUES (2, 3, 2, 1, 30, 120, 4);
INSERT INTO `cimerkisi`.`stanovanje` (`id_stanovanje`, `id_lokacija`, `id_tip_stanovanje`, `stroski_fiksni`, `stroski_vrednost`, `najemnina_vrednost`, `stevilo_sob`) VALUES (3, 5, 5, 1, 0, 0, 1);
INSERT INTO `cimerkisi`.`stanovanje` (`id_stanovanje`, `id_lokacija`, `id_tip_stanovanje`, `stroski_fiksni`, `stroski_vrednost`, `najemnina_vrednost`, `stevilo_sob`) VALUES (4, 4, 2, 1, 50, 300, 2);

/*-----------------------------------------------------------*/

INSERT INTO `cimerkisi`.`uporabnik_kriterij` (`id_uporabnik_kriterij`, `id_uporabnik`, `id_kriterij`) VALUES (1, 1, 5);
INSERT INTO `cimerkisi`.`uporabnik_kriterij` (`id_uporabnik_kriterij`, `id_uporabnik`, `id_kriterij`) VALUES (2, 2, 4);
INSERT INTO `cimerkisi`.`uporabnik_kriterij` (`id_uporabnik_kriterij`, `id_uporabnik`, `id_kriterij`) VALUES (3, 3, 3);
INSERT INTO `cimerkisi`.`uporabnik_kriterij` (`id_uporabnik_kriterij`, `id_uporabnik`, `id_kriterij`) VALUES (4, 4, 2);
INSERT INTO `cimerkisi`.`uporabnik_kriterij` (`id_uporabnik_kriterij`, `id_uporabnik`, `id_kriterij`) VALUES (5, 5, 2);
INSERT INTO `cimerkisi`.`uporabnik_kriterij` (`id_uporabnik_kriterij`, `id_uporabnik`, `id_kriterij`) VALUES (6, 6, 1);
INSERT INTO `cimerkisi`.`uporabnik_kriterij` (`id_uporabnik_kriterij`, `id_uporabnik`, `id_kriterij`) VALUES (7, 7, 1);
INSERT INTO `cimerkisi`.`uporabnik_kriterij` (`id_uporabnik_kriterij`, `id_uporabnik`, `id_kriterij`) VALUES (8, 8, 3);
INSERT INTO `cimerkisi`.`uporabnik_kriterij` (`id_uporabnik_kriterij`, `id_uporabnik`, `id_kriterij`) VALUES (9, 9, 4);
INSERT INTO `cimerkisi`.`uporabnik_kriterij` (`id_uporabnik_kriterij`, `id_uporabnik`, `id_kriterij`) VALUES (10, 10, 5);

/*-----------------------------------------------------------*/

INSERT INTO `cimerkisi`.`stanovanje_kriterij` (`id_stanovanje_kriterij`, `id_stanovanje`, `id_kriterij`) VALUES (1, 4, 5);
INSERT INTO `cimerkisi`.`stanovanje_kriterij` (`id_stanovanje_kriterij`, `id_stanovanje`, `id_kriterij`) VALUES (2, 3, 2);
INSERT INTO `cimerkisi`.`stanovanje_kriterij` (`id_stanovanje_kriterij`, `id_stanovanje`, `id_kriterij`) VALUES (3, 2, 3);
INSERT INTO `cimerkisi`.`stanovanje_kriterij` (`id_stanovanje_kriterij`, `id_stanovanje`, `id_kriterij`) VALUES (4, 1, 1);

/*-----------------------------------------------------------*/

INSERT INTO `cimerkisi`.`uporabnik_stanovanje` (`id_uporabnik_stanovanje`, `id_uporabnik`, `id_stanovanje`) VALUES (1, 1, 1);
INSERT INTO `cimerkisi`.`uporabnik_stanovanje` (`id_uporabnik_stanovanje`, `id_uporabnik`, `id_stanovanje`) VALUES (2, 2, 1);
INSERT INTO `cimerkisi`.`uporabnik_stanovanje` (`id_uporabnik_stanovanje`, `id_uporabnik`, `id_stanovanje`) VALUES (3, 3, 1);
INSERT INTO `cimerkisi`.`uporabnik_stanovanje` (`id_uporabnik_stanovanje`, `id_uporabnik`, `id_stanovanje`) VALUES (4, 4, 2);
INSERT INTO `cimerkisi`.`uporabnik_stanovanje` (`id_uporabnik_stanovanje`, `id_uporabnik`, `id_stanovanje`) VALUES (5, 5, 2);
INSERT INTO `cimerkisi`.`uporabnik_stanovanje` (`id_uporabnik_stanovanje`, `id_uporabnik`, `id_stanovanje`) VALUES (6, 6, 2);
INSERT INTO `cimerkisi`.`uporabnik_stanovanje` (`id_uporabnik_stanovanje`, `id_uporabnik`, `id_stanovanje`) VALUES (7, 7, 2);
INSERT INTO `cimerkisi`.`uporabnik_stanovanje` (`id_uporabnik_stanovanje`, `id_uporabnik`, `id_stanovanje`) VALUES (8, 8, 3);
INSERT INTO `cimerkisi`.`uporabnik_stanovanje` (`id_uporabnik_stanovanje`, `id_uporabnik`, `id_stanovanje`) VALUES (9, 9, 4);
INSERT INTO `cimerkisi`.`uporabnik_stanovanje` (`id_uporabnik_stanovanje`, `id_uporabnik`, `id_stanovanje`) VALUES (10, 10, 4);

/*-----------------------------------------------------------*/