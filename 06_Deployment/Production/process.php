<?php
	include_once("system/main.php");
	$user = new User($_SESSION['mail'], $_SESSION['pass']);

	
	if(isset($_GET['lbrw']) &&  (isset($_POST['zbrw']) || (isset($_GET['zbrw'])))) {
		if($_GET['lbrw'] == $TOKEN) {

			if(isset($_POST['zbrw'])) {
				$opcija = $_POST['zbrw'];
			} else {
				$opcija = $_GET['zbrw'];
			}

			switch($opcija) {
				case 1:
					//DELETE stanovanja
					if(isset($_GET['stan_id'])) {
						try {
							$stmt = $db->prepare("DELETE FROM stanovanje_2 WHERE id =? AND uporabnik_id =?");
							$stmt->execute(array($_GET['stan_id'], $user->getData('id_uporabnik')));
							header("location:".$_baseURL."pregled_dodanih_stanovanj.php");
						} catch (PDOException $ex) {
							newError($ex->getMessage());
							header("location:".$_baseURL."message.php?msg=Napaka! Poskusi ponovno.&src=pregled_dodanih_stanovanj.php");
						}
					}
				break;
				case 2:
					$type = "cimer";
					if(isset($_GET['type'])) {
						if(decrypt($_GET['type']) == "stan") {	$type = "stan";	}
					}

					try {
						$provjera = $db->prepare("SELECT * FROM kriterij WHERE sOc_ID=? AND sOc=?");
						$provjera->execute(array($user->getData("id_uporabnik"), "cimer"));
						$broji = $provjera->rowCount();
						if($broji == 0) {
							$stmtX = $db->prepare("INSERT INTO kriterij (sOc, sOc_ID) VALUES (:field1, :field2)");
							$stmtX->execute(array($type, $user->getData("id_uporabnik")));
						} 					
						//header("location:".$_baseURL."pregled_dodanih_stanovanj.php");
					} catch (PDOException $ex) {
						newError($ex->getMessage());
						header("location:".$_baseURL."message.php?msg=Napaka! Poskusi ponovno.&src=najdi_cimra.php");
					}

					$stmt = $db->prepare("DESCRIBE kriterij");
					$stmt->execute();
					
					while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
						$temp_str = explode("_", $row['Field']);
						if($temp_str[0] == $type) {
							$type_temp = $type."_";
							$str_main = str_replace($type_temp, "", $row['Field']);
							$str_main_temp = str_replace("_", " ", $str_main);
							$str_main_temp = ucfirst($str_main_temp);
							
							$kriterij = $type."_".$str_main;
							if(isset($_POST[$str_main])) {
								try {
									$stmtU_query = "UPDATE kriterij SET " . $kriterij . "=? WHERE sOc_ID=? AND sOc=?";
									$stmtU = $db->prepare($stmtU_query);
									$stmtU->execute(array(1, $user->getData("id_uporabnik"), "cimer"));
								} catch (PDOException $ex) {
									newError($ex->getMessage());
									header("location:".$_baseURL."message.php?msg=Napaka! Poskusi ponovno.&src=najdi_cimra.php");
								}
							} else {
								try {
									$stmtU_query = "UPDATE kriterij SET " . $kriterij . "=? WHERE sOc_ID=? AND sOc=?";
									$stmtU = $db->prepare($stmtU_query);
									$stmtU->execute(array(0, $user->getData("id_uporabnik"), "cimer"));
								} catch (PDOException $ex) {
									newError($ex->getMessage());
									header("location:".$_baseURL."message.php?msg=Napaka! Poskusi ponovno.&src=najdi_cimra.php");
								}
							}
						
						}
                			}
					if(isset($_GET['type'])) {
						if(decrypt($_GET['type']) == "stan") {
							echo "TEST";
							
							$st_sob = $_POST['st_sob'];
							$st_postelj = $_POST['st_postelj'];
							$stroski_max = $_POST['stroski_max'];
							$stroski_varscina = $_POST['stroski_varscina'];
							$velikost_stanovanja = $_POST['velikost_stanovanja'];
							$mesto = $_POST['mesto'];
							try {
								$provjera = $db->prepare("SELECT * FROM kriterij_ostalo WHERE id_uporabnik=?");
								$provjera->execute(array($user->getData("id_uporabnik")));
								$broji = $provjera->rowCount();
								if($broji == 0) {
									$stmtX = $db->prepare("INSERT INTO kriterij_ostalo (id_uporabnik) VALUES (:field1)");
									$stmtX->execute(array($user->getData("id_uporabnik")));
								} 					
							} catch (PDOException $ex) {
								newError($ex->getMessage());
								header("location:".$_baseURL."message.php?msg=Napaka! Poskusi ponovno.&src=iskanje_stanovanja.php");
							}

							try {
								$update = $db->prepare("UPDATE kriterij_ostalo SET st_sob=?, st_postelj=?, stroski_max=?, stroski_varscina=?, velikost_stanovanja=?, mesto=? WHERE id_uporabnik=?");
								$update->execute(array($st_sob, $st_postelj, $stroski_max, $stroski_varscina, $velikost_stanovanja, $mesto, $user->getData("id_uporabnik")));
							} catch (PDOException $ex) {
								newError($ex->getMessage());
								header("location:".$_baseURL."message.php?msg=Napaka! Poskusi ponovno.&src=iskanje_stanovanja.php");
							}
							
							header("location:".$_baseURL."iskanje_stanovanja.php");
						} else {
							header("location:".$_baseURL."najdi_cimra.php");
						}
					}
				break;
				case 3:
					//DELETE stanovanja
					if(isset($_GET['stan_id'])) {
						try {
							$stmt = $db->prepare("DELETE FROM prijavljeni WHERE stanovanje_id =? AND uporabnik_id =?");
							$stmt->execute(array($_GET['stan_id'], $user->getData('id_uporabnik')));
							header("location:".$_baseURL."pregled_dodanih_stanovanj.php");
						} catch (PDOException $ex) {
							newError($ex->getMessage());
							header("location:".$_baseURL."message.php?msg=Napaka! Poskusi ponovno.&src=pregled_dodanih_stanovanj.php");
						}
					}
				break;
				case 4:
					if((isset($_POST['slikaUrl']))&&(isset($_GET['stan_id']))) {
						try {
							$type = "main";
							$stmt0 = $db->prepare("SELECT * FROM stanovanje_2_slike WHERE type=? AND stanovanje_id=?");
							$stmt0->execute(array("main", decrypt($_GET['stan_id'])));
							$rowCount = $stmt0->rowCount();
							if($rowCount != 0) {	$type = ""; }
							$stmt = $db->prepare("INSERT INTO stanovanje_2_slike (stanovanje_id, src, type) VALUES (?, ?, ?)");
							$stmt->execute(array(decrypt($_GET['stan_id']), $_POST['slikaUrl'], $type));
							header("location:".$_baseURL."stanovanje_slike.php?stan_id=".decrypt($_GET['stan_id']));
						} catch (PDOException $ex) {
							newError($ex->getMessage());
							header("location:".$_baseURL."message.php?msg=Napaka! Poskusi ponovno.&src=pregled_dodanih_stanovanj.php");
						}
					}
				break;
				case 5:
					if((isset($_GET['stan_id']))&&(isset($_GET['id']))) {
						try {
							$stmt = $db->prepare("DELETE FROM stanovanje_2_slike WHERE ID=?");
							$stmt->execute(array(decrypt($_GET['id'])));
							header("location:".$_baseURL."stanovanje_slike.php?stan_id=".decrypt($_GET['stan_id']));
						} catch (PDOException $ex) {
							newError($ex->getMessage());
							header("location:".$_baseURL."message.php?msg=Napaka! Poskusi ponovno.&src=pregled_dodanih_stanovanj.php");
						}
					}
				break;
				case 6:
					if((isset($_GET['stan_id']))&&(isset($_GET['id']))) {
						try {	
							$stmt0 = $db->prepare("UPDATE stanovanje_2_slike SET type=? WHERE stanovanje_id=?");
							$stmt0->execute(array("", decrypt($_GET['stan_id'])));
							$stmt = $db->prepare("UPDATE stanovanje_2_slike SET type=? WHERE ID=?");
							$stmt->execute(array("main", decrypt($_GET['id'])));
							header("location:".$_baseURL."stanovanje_slike.php?stan_id=".decrypt($_GET['stan_id']));
						} catch (PDOException $ex) {
							newError($ex->getMessage());
							header("location:".$_baseURL."message.php?msg=Napaka! Poskusi ponovno.&src=pregled_dodanih_stanovanj.php");
						}
					}
				break;
				case 7:
					if((isset($_POST['slikaUrl']))&&(isset($_GET['id']))) {
						try {
							$stmt = $db->prepare("UPDATE uporabnik SET slika =? WHERE id_uporabnik =?");
							$stmt->execute(array($_POST['slikaUrl'], decrypt($_GET['id'])));
							header("location:".$_baseURL."profil.php");
						} catch (PDOException $ex) {
							newError($ex->getMessage());
							header("location:".$_baseURL."message.php?msg=Napaka! Poskusi ponovno.&src=pregled_dodanih_stanovanj.php");
						}
					}
				break;
			}
		}
	}

?>
