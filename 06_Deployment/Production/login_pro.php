<?php

	include_once("system/main.php");

	$username = $_POST['form-username'];
	$password = $_POST['form-password']; 
	function prijavaUpdate($mail, $status) {
		global $db;
		try {
			$stmt = $db->prepare("INSERT INTO prijave (mail, status) VALUES (:field1, :field2)");
			$stmt->execute(array(":field1" => $mail, ":field2" => $status));	
		} catch (PDOException $ex) {
			newError($ex->getMessage());
		}
	}

	try {
		$stmt = $db->prepare("SELECT * FROM uporabnik WHERE mail =? LIMIT 1");
		$stmt->execute(array($username));	
	} catch (PDOException $ex) {
		newError($ex->getMessage());
		prijavaUpdate($username, "FAILURE | greska kod 1 citanja iz baze");
		header("location:".$_baseURL."message.php?msg=Napaka! Poskusi ponovno.&src=login.php");
	}

	$rowCount = $stmt->rowCount();
	if($rowCount > 0) {
		$data = $stmt->fetch(PDO::FETCH_ASSOC);
		$salt = $data['salt'];
		
		$pass_in = MD5(encrypt($username) . $password . $salt);
		if($pass_in == $data['geslo']) {
			if($data['dozvola_prijave'] == 1) {
				//PRIJAVLJEN
				$_SESSION["mail"] = encrypt($username);
				$_SESSION["pass"] = encrypt($pass_in);

				prijavaUpdate($username, "SUCCESS");
				header("location:".$_baseURL."profil.php");	
			} else {
				prijavaUpdate($username, "FAILURE | nema dozvole prijave");
				header("location:".$_baseURL."message.php?msg=Napaka pri prijavi! Prosimo, potrdite prijavo s klikom na povezavo, ki smo vam jo poslali.&src=login.php");
			}
		} else {
			prijavaUpdate($username, "FAILURE | kriva lozinka");
			/*header("location:".$_baseURL."profil_uporabnika.php");*/
			// spodaj je prav, zakomentirano zato, ker ni konekcije z bazo
			header("location:".$_baseURL."message.php?msg=Napaka pri prijavi! Poskusite ponovno.&src=login.php");
		}

	} else {	
		prijavaUpdate($username, "FAILURE | nije registrirani");
		/*header("location:".$_baseURL."profil_uporabnika.php");*/
		// spodaj je prav, zakomentirano zato, ker ni konekcije z bazo
		header("location:".$_baseURL."message.php?msg=Napaka pri prijavi! Niste še registrirani.&src=registracija.php");
	}
?>
