<?php

	include_once("system/main.php");
	$user = new User($_SESSION['mail'], $_SESSION['pass']);

	$ulica = $_POST['ulica'];				//REQUIRED
	$hisna_st = $_POST['hisna_stevilka'];			//REQUIRED
	$mesto = $_POST['mesto'];				//REQUIRED
	$postna_st = $_POST['postna_stevilka'];			//REQUIRED

	$br_soba = $_POST['br_soba'];
	$br_kreveta = $_POST['br_kreveta'];
	$br_mesta = $_POST['br_mesta'];

	$vel_stana = $_POST['velikost_stanovanja'];		//REQUIRED
	$opis_stana = $_POST['opis_stanovanja'];

	$stroski_najamnine = $_POST['stroski_najamnine'];	//REQUIRED
	$stroski_obratovalni = $_POST['stroski_obratovalni'];	//REQUIRED
	$stroski_vrsta = $_POST['stroski_vrsta'];		//REQUIRED
	$stroski_varscina = $_POST['stroski_varscina'];		//REQUIRED

	$kriteriji = array();
	//KRITERIJI
	$stmt = $db->prepare("DESCRIBE kriterij");
	$stmt->execute();
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		$temp_str = explode("_", $row['Field']);
		if($temp_str[0] == "stan") {
			$str_main = str_replace("stan_", "", $row['Field']);
			$kriteriji[$i++] = $_POST[$str_main];
		}
	}


	if(isset($ulica)) {
		$stanovanje = new Stanovanje();
			$lokacija = array($ulica, $hisna_st, $mesto, $postna_st);
			$info = array($br_soba, $br_kreveta, $br_mesta, $vel_stana, $opis_stana);
			$stroski = array($stroski_najamnine, $stroski_obratovalni, $stroski_vrsta, $stroski_varscina);
		$stanovanje->newStanovanje($user->getData('id_uporabnik'), $info, $lokacija, $kriteriji, $stroski);
	}

?>
