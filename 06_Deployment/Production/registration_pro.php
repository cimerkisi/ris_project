<?php

	include_once("system/main.php");
	global $db;
	$ime = $_POST['ime'];
	$prezime = $_POST['priimek'];
	$email = $_POST['email'];
	$pass1 = $_POST['geslo'];
	$pass2 = $_POST['geslo_potrditev'];



/*	$id_tip_uporabnik = 1;
	$id_lokacija = 0;
	$status = "";
	$dozvola_prijave = 0;
	$tel_st = 0;
*/

	if($pass1 == $pass2) {
		
		$stmt1 = $db->prepare("SELECT mail FROM uporabnik WHERE mail =?");
		$stmt1->execute(array($email));
		$stmt1_rows = $stmt1->rowCount();

		if($stmt1_rows == 0) {	
			//registracija
			$salt = rand_string(50);
			$encryptedMail = encrypt($email);
			$pass = MD5($encryptedMail . $pass1 . $salt);
			$token = createTokenFor("uporabnik", "token");
	
			try {
				$stmt_reg = $db->prepare("INSERT INTO uporabnik (ime, priimek, mail, geslo, salt, status, dozvola_prijave, token) VALUES (:field1, :field2, :field3, :field4, :field5, :field6, :field7, :field8)");
				$stmt_reg->execute(array(":field1" => $ime, ":field2" => $prezime, ":field3" => $email, ":field4" => $pass, ":field5" => $salt, ":field6" => "NO-AUTH", ":field7" => "0", ":field8" => $token));
			} catch (PDOException $ex) {
				newError($ex->getMessage());
				header("location:".$_baseURL."message.php?msg=Napaka! Poskusi ponovno.&src=registracija.php");
			}

			$mail_subject = "Registracija";
			$mail_msg = "Za autentifikaciju pritisniti na <a href=\"".$_baseURL."auth.php?m=".encrypt($email)."&t=".$token."\">ovaj link</a>.";
			
			$mail_send = new MailSender($email, $mail_subject, $mail_msg);
			if($mail_send == 0) {
				newError("E-mail nije poslani na " . $email);
				header("location:".$_baseURL."message.php?msg=Napaka! Poskusi ponovno.&src=registracija.php");
			}
			header("location:".$_baseURL."message.php?msg=Za dokončanje registracije odprite povezavo v e-mailu (".$email."), ki smo vam ga poslali.&src=login.php");
			

		} else {
			header("location:".$_baseURL."message.php?msg=Vnešeni e-mail že obstaja.&src=registracija.php");
		}

	} else {
		header("location:".$_baseURL."message.php?msg=Gesli se ne ujemata.&src=registracija.php");
	}

	

?>
