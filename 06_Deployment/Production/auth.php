<?php

	include_once("system/main.php");

	$mail = decrypt($_GET['m']);
	$token = $_GET['t'];
	try {
		$stmt = $db->prepare("SELECT * FROM uporabnik WHERE mail =? AND token =?");
		$stmt->execute(array($mail, $token));
	} catch (PDOException $ex) {
		newError($ex->getMessage());
		header("location:".$_baseURL."message.php?msg=Napaka! Poskusi ponovno.&src=registracija.php");
	}
	$rowCount = $stmt->rowCount();

	if($rowCount > 0) {

		try {
			$stmt2 = $db->prepare("UPDATE uporabnik SET dozvola_prijave =?, status =? WHERE mail =? AND token =?");
			$stmt2->execute(array("1", "AUTH-SUCCESS", $mail, $token));
		} catch (PDOException $ex) {
			newError($ex->getMessage());
			header("location:".$_baseURL."message.php?msg=Napaka! Poskusi ponovno.&src=registracija.php");
		}

		header("location:".$_baseURL."message.php?msg=Registracija je uspela..&src=login.php");
		
		
	}
?>
