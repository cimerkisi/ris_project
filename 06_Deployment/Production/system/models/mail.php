<?php

        class MailSender {

                var $from = "Cimer ki si? <cimerkisi@cwebspace.com>";
                var $reply_to = "Cimer ki si? <cimerkisi@cwebspace.com>";

                var $to = "";
                var $subject = "";
                var $message = "";
                var $headers = "";

                function __construct($to, $subject, $message, $salji=1) {

                        $this->to = $to;
                        $this->subject = $subject;
                        $this->message = $message;

                        if($salji == 1) {
                                $status = $this->sendMail();
                                return $status;
                        }
                }

                function sendMail() {
                        $this->createHeader();
                        $provjera = mail($this->to, $this->subject, $this->message, $this->headers);
                        return $provjera;
                }

                function setFrom($from) {
                        $this->from = $from;
                }

                function setReplyTo($reply_to) {
                        $this->reply_to = $reply_to;
                }

                function createHeader() {
                        $this->headers = 'From: '. $this->from . "\r\n" .
                                            'Reply-To: ' . $this->reply_to . "\r\n" .
                                            "Content-Type: text/html; charset=UTF-8\r\n" .
                                            'X-Mailer: PHP/' . phpversion();


                }

        }

?>
