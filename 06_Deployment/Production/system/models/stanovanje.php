<?php

class Stanovanje {

	function __construct() {
		
	}

	function newStanovanje($user_id, $info, $lokacija, $kriteriji, $stroski) {
		//$id_lokacija = $this->newLokacija($lokacija);
		$this->unosStanovanja($user_id, $info, $lokacija, $kriteriji, $stroski);
	
			
	}

	function newLokacija($lokacija) {
		global $db;

		$mesto = $lokacija[0];
		$postna_st = $lokacija[1];
		$naslov = $lokacija[2];
		$id = novi_id("lokacija", "id_lokacija");
		
		

		try {
			$stmt = $db->prepare("INSERT INTO lokacija VALUES (:field1, :field2, :field3, :field4)");
			$stmt->execute(array(":field1" => $id,":field2" => $mesto,":field3" => $postna_st,":field4" => $naslov));
		} catch (PDOException $ex) {
			newError($ex->getMessage());
			header("location:".$_baseURL."message.php?msg=Napaka! Poskusi ponovno.&src=dodajanje_stanovanja.php");
		}
	
		return $id;	
	}

	function unosStanovanja($user_id, $info, $lokacija, $kriteriji, $stroski) {	
		global $db, $_baseURL;
		try {
			$stmt = $db->prepare("INSERT INTO stanovanje_2 (uporabnik_id, ulica, hisna_stevilka, mesto, postna_stevilka, stevilo_sob, stevilo_postelj, stevilo_prostih_mest, velikost_stanovanja, opis, najemnina, obratovalni_stroski, vrsta_stroskov, varscina) VALUES (:field0, :field1, :field2, :field3, :field4, :field5, :field6, :field7, :field8, :field9, :field10, :field11, :field12, :field13)");
			$stmt->execute(
				array(
					":field0" => $user_id,
					":field1" => $lokacija[0],
					":field2" => $lokacija[1],
					":field3" => $lokacija[2],
					":field4" => $lokacija[3],
					":field5" => $info[0],
					":field6" => $info[1],
					":field7" => $info[2],
					":field8" => $info[3],
					":field9" => $info[4],
					":field10" => $stroski[0],
					":field11" => $stroski[1],
					":field12" => $stroski[2],
					":field13" => $stroski[3]
				)
			);
			$stan_id = $db->lastInsertId();

				$stmt2_query = "INSERT INTO kriterij (sOc, sOc_ID";
				$stmt_k = $db->prepare("DESCRIBE kriterij");
				$stmt_k->execute();
				$i = 2;	
				while($row_k = $stmt_k->fetch(PDO::FETCH_ASSOC)) {

					$temp_str = explode("_", $row_k['Field']);
					if($temp_str[0] == "stan") {
						$i++;
						$stmt2_query .= ", ".$row_k['Field'];
					}
                                    
                                }
				$stmt2_query .= ") VALUES (:field0, :field1";
				
				for($j = 2; $j < $i; $j++) {
					$stmt2_query .= ", :field".$j;
				}
				$stmt2_query .= ")";
				
			$stmt2 = $db->prepare($stmt2_query);
			$stmt2_array = array("stan", $stan_id);
			foreach($kriteriji as $kriterij) {
				if(isset($kriterij)) {	$kriterij = 1;	} else {	$kriterij = 0;	}
				array_push($stmt2_array, $kriterij);
			}
			$stmt2->execute($stmt2_array);
			header("location:".$_baseURL."pregled_dodanih_stanovanj.php");
		} catch (PDOException $ex) {
			newError($ex->getMessage());
			header("location:".$_baseURL."message.php?msg=Napaka! Poskusi ponovno.&src=dodajanje_stanovanja.php");
		}
	}

}

?>
