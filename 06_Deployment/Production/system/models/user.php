<?php

//OVA SE KORISTI ZA LOGINANOG(trenutnog) USERA	- pr. provjera prijave
class User {


	var $ID = "";
	var $ime = "";
	var $prezime = "";
	var $email = "";
	//var $password = "";
	var $status = "";
	var $mogucnost_prijave = "";

	var $user_data = array();

	var $kriteriji;
	var $kriterijiOstalo;

	function __construct($username, $password){

	global $db;

		$username = decrypt($username);
		$password = decrypt($password);

		$stmt = $db->prepare("SELECT * FROM uporabnik WHERE mail=? AND geslo=?");
			$stmt->execute(array($username, $password));
			$count = $stmt->rowCount();

		if($count != 1)
		{
			error_reporting(0);		
			session_destroy();
			link_win("Krivo korisničko ime ili zaporka. Pokušajte ponovo", "index.php");		
		}
		else
		{
			$stmt = $db->prepare("SELECT * FROM uporabnik WHERE mail=? AND geslo=?");
				$stmt->execute(array($username, $password));
				$user =$stmt->fetch(PDO::FETCH_ASSOC);
			$this->user_data = $user;
			$this->ID = $user['id_uporabnik'];
			$this->ime = $user['ime'];
			$this->prezime = $user['priimek'];
			$this->email = $user['mail'];
			$this->status = $user['status'];
			$this->mogucnost_prijave = $user['dozvola_prijave'];
			
		}

		$this->fetchDataKriterij();
		$this->fetchDataKriterijOstalo();
	}

	function getID() {	return $this->ID;	}
	function getIme() {	return $this->ime;	}
	function getPrezime() {	return $this->prezime;	}
	function getEmail() {	return $this->email;	}
	function getStatus() {	return $this->status;	}
	function getPrijava() {	return $this->mogucnost_prijave;	}
	function getData($data) {	return $this->user_data[$data];	}
	function getLokacija() {
		
	}
	
	function fetchDataKriterij() {
		global $db;
		$stmt = $db->prepare("SELECT * FROM kriterij WHERE sOc_ID=? AND sOc=? LIMIT 1");
		$stmt->execute(array($this->ID, "cimer"));
		$this->kriteriji = $stmt->fetch(PDO::FETCH_ASSOC);
	
	}

	function fetchDataKriterijOstalo() {
		global $db;
		$stmt = $db->prepare("SELECT * FROM kriterij_ostalo WHERE id_uporabnik=? LIMIT 1");
		$stmt->execute(array($this->ID));
		$this->kriterijiOstalo = $stmt->fetch(PDO::FETCH_ASSOC);
	
	}


	function getDataKriterij($name) {
		return $this->kriteriji[$name];
	}
	function getDataKriterijOstalo($name) {
		return $this->kriterijiOstalo[$name];
	}
//	function getProvjera() {	return $this->prijavljen;	}

}


?>
