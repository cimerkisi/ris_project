<?php

//////////////////////Kreiranje stringa (npr. za salt)

function rand_string($length) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $size = strlen( $chars );
    for( $i = 0; $i < $length; $i++ ) {
        $str .= $chars[ rand( 0, $size - 1 ) ];
    }
    return $str;
}

///////////////////////////////
//funkcije za encrypt i decrypt
            $salt ='a2o09v8jo4dop142jco';

    function encrypt($text)
    {
        
        $data = trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
        $data = str_replace(array('+','/','='),array('-','_','$'),$data);
        return $data;
    }           
                
    function decrypt($text)
    {           
        $text = str_replace(array('-','_','$'),array('+','/','='),$text);
        return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $salt, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
    }



function createTokenFor($table, $column) {
        global $db;
        $token = rand_string(50);
                $sql = "SELECT COUNT(*) FROM " . $table . " WHERE " . $column . " =?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($token));

        $counts = $stmt->fetch(PDO::FETCH_ASSOC);
        if($counts['COUNT(*)'] != 0) {
                createTokenFor($table, $column);
        } else {
                return $token;
        }
}

function newError($msg) {
        global $db;
        $stmt = $db->prepare("INSERT INTO errors (msg) VALUES (:field1)");
        $stmt->execute(array(":field1" => $msg));
}

function link_win($poruka, $link)
{
	print "<script type=\"text/javascript\">";
        print "alert('".$poruka."');";
        print "window.location.href = '".$link."'";
        print "</script>";
}

function novi_id($tab, $col)
{
	global $db;
	$brid = 0;

	$stmt = $db->prepare("SELECT MAX(".$col.") FROM ".$tab);
		$stmt->execute();
		$rows = $stmt->fetch(PDO::FETCH_ASSOC);
		$temp_str = "MAX(".$col.")";
		$brid = $rows[$temp_str];
	
        return ++$brid;
}


?>
