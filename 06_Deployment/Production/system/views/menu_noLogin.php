<?php

	class Menu_noLogin {

		var $menu_link_1 = "login.php";
		var $menu_link_1_str = "Prijava";
		var $menu_link_2 = "registracija.php";
		var $menu_link_2_str = "Registracija";
		

		function __construct() {
			
				//$this->menu_link_1 = "user.php";	//TODO
				$this->menu_link_1 = "login.php";	//TODO
				$this->menu_link_1_str = "Prijava";
				$this->menu_link_2 = "registracija.php";
				$this->menu_link_2_str = "Registracija";
				
			
		}
		
		function __toString() {
			global $_baseURL;
			return '
    			<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
		        <div class="container-fluid">
	                <div class="navbar-header">
		                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		                    <span class="sr-only">Toggle navigation</span>
	        	            <span class="icon-bar"></span>
	                	    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>
	        	        <a class="navbar-brand page-scroll" href="'.$_baseURL.'index.php"><img src="'.$_baseURL.'src/img/logo7.png" class="img-responsive"></a>
			</div>
		            <!-- Collect the nav links, forms, and other content for toggling -->
		            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		                <ul class="nav navbar-nav navbar-right">
						<li>
		                        <a class="page-scroll" href="'.$_baseURL.'index.php">Domov</a>
		                    </li>
		                    <li>
		                        <a href="'.$_baseURL.$this->menu_link_1.'">'.$this->menu_link_1_str.'</a>
		                    </li>
		                    <li>
		                        <a href="'.$_baseURL.$this->menu_link_2.'">'.$this->menu_link_2_str.'</a>
		                    </li>
		                    
		                </ul>
		            </div>
		            <!-- /.navbar-collapse -->
		        </div>
		        <!-- /.container-fluid -->
			</nav>';
		}

	}

?>
