<?php

class IdealnaStanovanja {

		function __construct() {}

		function __toString() {
			global $_baseURL;
			global $TOKEN;
			global $user;
			global $db;
			$result = '

<body class="">


<!-- NAVBAR CODE END -->
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h2>Meni</h2>
				<br> <br>

			</div>
		</div>';

		$style1 = "background-color:#8b2989;color:#fff";
		$style2 = "background-color:#28d14f;color:#333";
		$style3 = "background-color:#FFFFFF;color:#333";

		$result_temp0 = "";
		$result_temp1 = "";
		$result_temp2 = "";

		$stmt = $db->prepare("SELECT * FROM stanovanje_2");
		$stmt->execute();
		$i = 0;
		$stanOcena = 5;
		while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			$slika = $db->prepare("SELECT * FROM stanovanje_2_slike WHERE type=? AND stanovanje_id=? LIMIT 1");
			$slika->execute(array("main", $row['id']));
			$slika = $slika->fetch(PDO::FETCH_ASSOC);
			$slika = $slika['src'];

			$moj_stan = $db->prepare("SELECT * FROM stanovanje_2 WHERE id=? AND uporabnik_id=?");
			$moj_stan->execute(array($row['id'], $user->getData('id_uporabnik')));
			$moj_stan = $moj_stan->rowCount();
			if($moj_stan == 0) {
			
				//SEARCH
					$kriterij = 1;
					if($user->getDataKriterijOstalo("mesto") == $row['mesto']) {
						$myKriterij_stan = $db->prepare("SELECT * FROM kriterij WHERE sOc =? AND sOc_id=?");
						$myKriterij_stan->execute(array("cimer", $user->getData("id_uporabnik")));
						$myKriterij_stan = $myKriterij_stan->fetch(PDO::FETCH_ASSOC);
						
						$Kriterij_stan = $db->prepare("SELECT * FROM kriterij WHERE sOc =? AND sOc_id=?");
						$Kriterij_stan->execute(array("stan", $row['id']));
						$Kriterij_stan = $Kriterij_stan->fetch(PDO::FETCH_ASSOC);
						
						$diffs = array_diff($myKriterij_stan, $Kriterij_stan);
						foreach($diffs as $diff) {
							$stanOcena -= 0.2;
						}

					} else {
						$kriterij = 0;
					}
				//SEARCH - end

	
				if($kriterij == 1) {
					$style = $style2;
		
					$prijavljeni = $db->prepare("SELECT * FROM prijavljeni WHERE stanovanje_id =? AND status =?");
					$prijavljeni->execute(array($row['id'], "ACCEPTED"));
					$prijavljeni = $prijavljeni->rowCount();
					
					$result_temp0 .= '
					<!-- REZULTAT STANOVANJA-->
					<div class="col-xs-12 col-md-4 col-lg-4" style="margin-bottom: 20px">
					  <div class="panel panel-default">
					    <div class="panel-heading" style="'.$style.'">
					      <h4 class="panel-title">'.$row['mesto'].'</h4>
					      <p class="panel-title"><code>'.$row['ulica'].'</code></p>
					    </div>
					    <div class="panel-body">
						<img src="'.$slika.'" height="230px" width="100%">
						<button type="button" class="btn btn-circle" style="background-color:#28d14f; margin-top: 10px">
						<span class="fa fa-users"></span> '.$prijavljeni.'/'.$row['stevilo_prostih_mest'].'
						</button>
						<button type="button" class="btn btn-circle" style="background-color:#28d14f; margin-top: 10px">';
							if($stanOcena > 4.5) {
								for($i = 0; $i < 5; $i++) {
									$result_temp0 .= '<span class="fa fa-star"></span>';
								}
							} else if(($stanOcena <= 4.5)&&($stanOcena > 4)) {
								for($i = 0; $i < 4; $i++) {
									$result_temp0 .= '<span class="fa fa-star"></span>';
								}
								$result_temp0 .= '<span class="fa fa-star-half-o"></span>';
							}  else if(($stanOcena <= 4)&&($stanOcena > 3.5)) {
								for($i = 0; $i < 3; $i++) {
									$result_temp0 .= '<span class="fa fa-star"></span>';
								}
								$result_temp0 .= '<span class="fa fa-star-half-o"></span>';
								$result_temp0 .= '<span class="fa fa-star-o"></span>';
							}  else if(($stanOcena <= 3.5)&&($stanOcena > 3)) {
								for($i = 0; $i < 3; $i++) {
									$result_temp0 .= '<span class="fa fa-star"></span>';
								}
								$result_temp0 .= '<span class="fa fa-star-o"></span>';
								$result_temp0 .= '<span class="fa fa-star-o"></span>';
							}  else if(($stanOcena <= 3)&&($stanOcena > 2.5)) {
								for($i = 0; $i < 2; $i++) {
									$result_temp0 .= '<span class="fa fa-star"></span>';
								}
								$result_temp0 .= '<span class="fa fa-star-half-o"></span>';
								for($i = 0; $i < 2; $i++) {
									$result_temp0 .= '<span class="fa fa-star-o"></span>';
								}
							}  else if(($stanOcena <= 2.5)&&($stanOcena > 2)) {
								for($i = 0; $i < 2; $i++) {
									$result_temp0 .= '<span class="fa fa-star"></span>';
								}
								for($i = 0; $i < 3; $i++) {
									$result_temp0 .= '<span class="fa fa-star-o"></span>';
								}
							} else {	
								for($i = 0; $i < 2; $i++) {
									$result_temp0 .= '<span class="fa fa-star"></span>';
								}
								for($i = 0; $i < 3; $i++) {
									$result_temp0 .= '<span class="fa fa-star-o"></span>';
								}
							}
					$result_temp0 .= '
						</button>
					    </div>
					    <div class="panel-footer text-center"> <a href="'.$_baseURL.'profil_stanovanja.php?stan_id='.$row['id'].'"> Več informacij </a> </div>
					  </div>
					</div>
					<!-- KONEC STANOVANJA--> ';
				} else {
		
					$style = $style1;
					$prijavljeni = $db->prepare("SELECT * FROM prijavljeni WHERE stanovanje_id =? AND status =?");
					$prijavljeni->execute(array($row['id'], "ACCEPTED"));
					$prijavljeni = $prijavljeni->rowCount();
					
					$result_temp1 .= '
					<!-- REZULTAT STANOVANJA-->
					<div class="col-xs-12 col-md-4 col-lg-4" style="margin-bottom: 20px">
					  <div class="panel panel-default">
					    <div class="panel-heading" style="'.$style.'">
					      <h4 class="panel-title">'.$row['mesto'].'</h4>
					      <p class="panel-title"><code>'.$row['ulica'].'</code></p>
					    </div>
					    <div class="panel-body">
						<img src="'.$slika.'" height="230px" width="100%">
						<button type="button" class="btn btn-circle" style="background-color:#28d14f; margin-top: 10px">
						<span class="fa fa-users"></span> '.$prijavljeni.'/'.$row['stevilo_prostih_mest'].'
						</button>
						<button type="button" class="btn btn-circle" style="background-color:#28d14f; margin-top: 10px">
							<span class="fa fa-star"></span>
							<span class="fa fa-star-half-o"></span>
							<span class="fa fa-star-o"></span>
							<span class="fa fa-star-o"></span>
							<span class="fa fa-star-o"></span>
						</button>
					    </div>
					    <div class="panel-footer text-center"> <a href="'.$_baseURL.'profil_stanovanja.php?stan_id='.$row['id'].'"> Več informacij </a> </div>
					  </div>
					</div>
					<!-- KONEC STANOVANJA--> ';
				}
			} else {

	
				$style = $style3;
				$prijavljeni = $db->prepare("SELECT * FROM prijavljeni WHERE stanovanje_id =? AND status =?");
				$prijavljeni->execute(array($row['id'], "ACCEPTED"));
				$prijavljeni = $prijavljeni->rowCount();
				
				$result_temp2 .= '
				<!-- REZULTAT STANOVANJA-->
				<div class="col-xs-12 col-md-4 col-lg-4" style="margin-bottom: 20px">
				  <div class="panel panel-default">
				    <div class="panel-heading" style="'.$style.'">
				      <h4 class="panel-title">'.$row['mesto'].'</h4>
				      <p class="panel-title"><code>'.$row['ulica'].'</code></p>
				    </div>
				    <div class="panel-body">
					<img src="'.$slika.'" height="230px" width="100%">
					<button type="button" class="btn btn-circle" style="background-color:#28d14f; margin-top: 10px">
					<span class="fa fa-users"></span> '.$prijavljeni.'/'.$row['stevilo_prostih_mest'].'
					</button>
					<button type="button" class="btn btn-circle" style="background-color:#28d14f; margin-top: 10px">
						<span class="fa fa-home"></span>
					</button>
				    </div>
				    <div class="panel-footer text-center"> <a href="'.$_baseURL.'profil_stanovanja.php?stan_id='.$row['id'].'"> Več informacij </a> </div>
				  </div>
				</div>
				<!-- KONEC STANOVANJA--> ';
			}
		}


	$result .= $result_temp0;
	$result .= $result_temp1;
	$result .= $result_temp2;
	$result .= '
</div>


';
		return $result;
	}
}

?>
