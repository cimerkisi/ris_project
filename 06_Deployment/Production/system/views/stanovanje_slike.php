<?php
class stanovanje_slike {

		var $id;
		var $data;
		var $kriterij;
		var $myStan = 1;
		var $prijava = 0;

		function __construct($id) {
			$this->id = $id;
			$this->fetchData();
			$this->fetchDataKriterij();
			$this->checkIfStanMy();
			$this->checkIfPrijavljen();
		}

		function fetchData() {
			global $db;
			$stmt = $db->prepare("SELECT * FROM stanovanje_2 WHERE id=? LIMIT 1");
			$stmt->execute(array($this->id));
			$this->data = $stmt->fetch(PDO::FETCH_ASSOC);
		}

		function fetchDataKriterij() {
			global $db;
			$stmt = $db->prepare("SELECT * FROM kriterij WHERE sOc_ID=? AND sOc=? LIMIT 1");
			$stmt->execute(array($this->id, "stan"));
			$this->kriterij = $stmt->fetch(PDO::FETCH_ASSOC);
		}

		function getData($name) {
			return $this->data[$name];
		}

		function getDataKriterij($name) {
			return $this->kriterij[$name];
		}

		function checkIfStanMy() {
			global $user;
			global $db;

			$stmt = $db->prepare("SELECT * FROM stanovanje_2 WHERE id=? AND uporabnik_id=?");
			$stmt->execute(array($this->id, $user->getData('id_uporabnik')));
			$broji = $stmt->rowCount();
			if($broji == 0) {
				$this->myStan = 0;
			} else {
				$this->myStan = 1;
			}
		}

		function checkIfPrijavljen() {
			global $user;
			global $db;

			$stmt = $db->prepare("SELECT * FROM prijavljeni WHERE stanovanje_id=? AND uporabnik_id=?");
			$stmt->execute(array($this->id, $user->getData('id_uporabnik')));
			$broji = $stmt->rowCount();
			if($broji == 0) {
				$this->prijava = 0;
			} else {
				$this->prijava = 1;
			}
		}

		function __toString() {
			global $_baseURL;
			global $TOKEN;
			global $user;
			global $db;
			$gsi1 = "edit";
			$gsi2 = "Uredi podatke o stanovanju";
			$gsi3 = "Prijavljene in potrjene osebe";
			if($this->myStan == 0) {
				$gsi1 = "info";
				$gsi2 = "Podatki o stanovanju";
				$gsi3 = "Cimri";
			}
			$result = '
<body class="">

	<!-- NAVBAR CODE END -->
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h2>Meni</h2>

				<br> <br>

			</div>
		</div>
		<!-- USER PROFILE ROW STARTS-->
		<div class="row">
        			<!-- PODMENI ZA POSTAVKE -->
			<div class="col-md-12 col-sm-6  user-wrapper">

				<ul class="nav nav-pills success">
                
                
					<li><a href="profil_stanovanja.php?stan_id='.$this->id.'"><i class="fa fa-'.$gsi1.'"></i> '.$gsi2.'</a></li>
                    <li class="active"><a href="stanovanje_slike.php?stan_id='.$this->id.'"><i class="fa fa-image"></i> Fotogalerija</a></li>
					<li><a href="stanovanje_ideal_cimer.php?stan_id='.$this->id.'"><i class="fa fa-users"></i>
							'.$gsi3.'</a></li>';
				if($this->myStan == 0) {
					if($this->prijava == 1) {
						$result .= '<li class="active" id="prijavaLI"><a href="javascript::void(0)" onclick="prijavaStan(0)" id="prijavaButton" style="background-color: green"><i class="fa fa-check"></i> Prijavljen</a></li>';
					} else {
						$result .= '<li class="active" id="prijavaLI"><a href="javascript::void(0)" onclick="prijavaStan(1)" id="prijavaButton" style="background-color: orange"><i class="fa fa-sign-in"></i> Prijavi se</a></li>';
					}

					$result .= '
						<script>
							function prijavaStan(status) {
								$("#prijavaButton").html("<div style=\"width: 85px; padding-left: 35px\"><i class=\"fa fa-spinner fa-pulse\"></i></div>");
								var data0 = 2;
								var data1 = "'.encrypt($this->getData('id')).'";
								var data2 = "'.encrypt($user->getData('id_uporabnik')).'";
								
								$.ajax({
									type: "POST",
									url: "'.$_baseURL.'ajax.php?lbrw='.encrypt($TOKEN).'",
									async: false,
									data: { zbrw: data0, kbrw: status, pod1: data1, pod2: data2 },
									success: function(data) {
										$("#prijavaLI").html(data);  
										result = 1;
									},
									error: function(data) {
										console.log("Greska");
									}
								});
							}
						</script>
					';
					
				}
$result .= '				</ul>


			</div>
  
			<div class="col-md-12 col-sm-12  user-wrapper" style="padding-top:20px">
				<div class="description">
					<i class="fa fa-image fa-2x"> Fotografije</i>

					<hr>';

if($this->myStan == 1) {
$result .= '
	<form action="'.$_baseURL.'process.php?lbrw='.$TOKEN.'&zbrw=4&stan_id='.encrypt($this->id).'" method="post" name="form1">
		<div class="form-group col-md-9" style="padding-top: 5px">
			<label class="sr-only" for="mesto">Mesto</label>
			<input placeholder="URL slike" class="form-control" name="slikaUrl" type="text" required>
		</div>
		<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Dodaj fotografijo</button>
	</form>
    
    <hr>';
}

$result .= '
    
    <fieldset>';
	
	$stmt_slike = $db->prepare("SELECT * FROM stanovanje_2_slike WHERE stanovanje_id =? ORDER BY type DESC");
	$stmt_slike->execute(array($this->id));
	while($row_slike = $stmt_slike->fetch(PDO::FETCH_ASSOC)) {
		if($this->myStan == 1) {
			if($row_slike['type'] == "main") {
				$result .= '<div style="display: inline" class="pull-right"><i class="fa fa-star fa-2x" style="color:orange; padding-top: 5px"> </i></div>';
			} else {
				$result .= '<a href="'.$_baseURL.'process.php?lbrw='.$TOKEN.'&zbrw=6&stan_id='.encrypt($this->id).'&id='.encrypt($row_slike['ID']).'" class="btn btn-xs btn-warning pull-right" data-toggle="modal" type="button"><i class="fa fa-star fa-2x"> </i></a>';
			}
			$result .= '
			<a href="'.$_baseURL.'process.php?lbrw='.$TOKEN.'&zbrw=5&stan_id='.encrypt($this->id).'&id='.encrypt($row_slike['ID']).'" class="btn btn-xs btn-danger pull-left" data-toggle="modal" type="button"><i class="fa fa-trash fa-2x"> </i></a>';
		}

		$result .= '
	       <img style="width: 100%" src="'.$row_slike['src'].'"/>';
	}

	$result .= '
       
    </fieldset>
    
				</div>

			</div>


			
		</div>

	</div>



	<!-- USER PROFILE ROW END-->
	</div>
	<!-- CONATINER END -->



	<!-- REQUIRED SCRIPTS FILES -->
	<script data-rocketsrc="http://www.designbootstrap.com/track/ga.js"
		type="text/rocketscript" data-rocketoptimized="true"></script>
	<!-- CORE JQUERY FILE -->
	<script data-rocketsrc="assets/js/jquery-1.11.1.js"
		type="text/rocketscript" data-rocketoptimized="true"></script>
	<!-- REQUIRED BOOTSTRAP SCRIPTS -->
	<script data-rocketsrc="assets/js/bootstrap.js"
		type="text/rocketscript" data-rocketoptimized="true"></script>

</body>



';
			return $result;
		}
	}
?>
