<?php

//	class Registration extends MainView {
	class Stroski_stanovanja {

		function __construct() {}

		function __toString() {
			return '			
        <!-- Top content -->
		 <header>
        <div class="header-content">
            <div class="header-content-inner">
		
        <div class="top-content">
        	
            <div class="inner-bgS">
                <div class="container">
                    <div class="row">
                      <div class="col-sm-8 col-sm-offset-2 form-top form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Dodajanje stanovanja</h3>
                            	<h4>Stroški</h4>
                        		</div>
                        		<div class="form-top-right">
                                	<i class="fa fa-eur"></i>
                        		</div>
                            </div>
                            <div class="form-bottom" >
			                    <form role="form" action="registracija3.html" method="post" class="login-form">			
                               
                                
                                <div class="btn-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                <label class="sr-only" for="stevilo_sob">Število sob</label>
                                <select class="form-control btn btn-default">
                                <option value="fiksni">Fiksni</option>
                                <option value="spremenjivi">Spremenjivi</option>
                              
 								
                                </select>
                                </div>
			                      
			                        </div>
                                    
                                    <div class="form-top">
                                <div class="form-top-left">
                            	<h4>Stroški najamnine</h4>
                        		</div>
                                </div>
                                    
                                    <div class="input-group">
  <span class="input-group-addon">€</span>
  <input type="text" name="stroski_najamnine" class="form-control" aria-label="Amount (to the nearest dollar)">
  <span class="input-group-addon">.00</span>
</div>
                                   <div class="form-top">
                                <div class="form-top-left">
                            	<h4>Obratovalni stroški:</h4>
                        		</div>
                                </div>
                                    
                                    <div class="input-group">
  <span class="input-group-addon">€</span>
  <input type="text" name="obratovalni_stroski" class="form-control" aria-label="Amount (to the nearest dollar)">
  <span class="input-group-addon">.00</span>
                               
                                    
                                    
                                   </div>
                                   
                                    <div class="form-top">
                                <div class="form-top-left">
                            	<h4>Varščina:</h4>
                        		</div>
                                </div>
                                    
                                    <div class="input-group">
  <span class="input-group-addon">€</span>
  <input type="text" name="stroski_varscina" class="form-control" aria-label="Amount (to the nearest dollar)">
  <span class="input-group-addon">.00</span>
                               
                                    
                                    
                                   </div>
                                    
                                   <div class="btn-group col-lg-12" style="margin-top:20px">			
			             <div class="col-md-6 col-lg-3 col-sm-6 col-xs-6 col-lg-push-7 col-md-push-12 col-sm-push-7 col-xs-push-4">
                          <button class=" btn btn-danger" type="submit" class="btn btn-primary">Preskoči </button> 
               </div>
               <div class="col-md-6 col-lg-3 col-sm-6 col-xs-6 col-lg-push-7 col-md-push-12 col-sm-push-6 col-xs-push-3" >  
               <button class="btn btn-primary" type="submit" class="btn btn-primary">Nadaljuj</button>
									</div>
                                    </div>
			                    </form>
                    
		                    </div>
                  </div>
              </div>
          </div>
            </div>
            
        </div>
        </div>
        </div>
		</header>
			';
		}
	
	}

?>
