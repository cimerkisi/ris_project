<?php
class Pregled_dodanih_stanovanj{

		function __construct() {}

		function __toString() {
			global $_baseURL;
			global $db;
			global $TOKEN;
			global $user;
			$result = '


			<body class="">

	<!-- NAVBAR CODE END -->
	<div class="container">
		<div class="row">
			<div class="text-center">
				<h2>Meni</h2>

				<br> <br>

			</div>
		</div>

		<!-- USER PROFILE ROW STARTS-->
		<div class="row">

			<div class="col-md-12  user-wrapper"
				style="padding-bottom: 10px;">
				<div class="description">
					<div class="container-fluid">
						<i class="fa fa-list fa-2x"> </i>
						<t class="text-center h2"> Moja stanovanja </t>
						<hr>

						<br />


					</div>



					<div class="container-fluid">';



					$stmt = $db->prepare("SELECT * FROM stanovanje_2 WHERE uporabnik_id =?");
					$stmt->execute(array($user->getData('id_uporabnik')));
					
					while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
$result .= '						<!-- Stanovanje 1 primer -->
						<div class="panel-group">
						  <div class="panel panel-default">
						    <div class="panel-heading" style="max-height:130px;min-height:50px;">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" href="#stanovanje1" class="h3">'.$row['ulica'].' '.$row['hisna_stevilka'].', <code>'.$row['postna_stevilka'].' '.$row['mesto'].'</code></a>
							<div class="btn-group" role="group" aria-label="..." style="float: right;">
                                            		  <a href="stanovanje_ideal_cimer.php?stan_id='.$row['id'].'" class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="left" title="Upravljanje z stanovalci"><i class="fa fa-user fa-2x"> </i></a>
                                                          <a href="profil_stanovanja.php?stan_id='.$row['id'].'" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="left" title="Uređivanje podatki o stanovanju"><i class="fa fa-edit fa-2x"> </i></a>
                                                          <a href="#" class="btn btn-xs btn-danger"  data-toggle="modal" data-target="#brisanjeStanovanja" type="button"><i class="fa fa-remove fa-2x"> </i></a>
       
                                           
									<!-- Modal -->
									<div id="brisanjeStanovanja" class="modal fade" role="dialog">
									  <div class="modal-dialog">
									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">Brisanje stanovanja</h4>
									      </div>
									      <div class="modal-body">
										        <p>Ste prepričani, da želite izbrisati stanovanje?</p>
									      </div>
									      <div class="modal-footer">
									        <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="window.location.href=\''.$_baseURL.'process.php?lbrw='.$TOKEN.'&zbrw=1&stan_id='.$row['id'].'\'">Izbriši</button>
									        <button type="button" class="btn btn-default" data-dismiss="modal">Zapri</button>
									      </div>
									    </div>
									  </div>
									</div>		
				                                        <!-- modal konec-->
							</div>
						      </h4>
						    </div>
							<!--<div id="stanovanje1" class="panel-collapse collapse">
								<div class="panel-body">Informacije o stanovanju</div>
							</div>-->
						</div>
						</div>
						<!-- Stanovanje 1 konec -->';
					}
$result .= '					</div>
				</div>
			</div>
		</div>
			<div class="col-md-12  user-wrapper"
				style="padding-bottom: 10px;">
				<div class="description">
					<div class="container-fluid">
						<i class="fa fa-list fa-2x"> </i>
						<t class="text-center h2"> Prijavljen na stanovanja </t>
						<hr>

						<br />


					</div>';



				$stmt2 = $db->prepare("SELECT * FROM prijavljeni WHERE uporabnik_id =?");
				$stmt2->execute(array($user->getData('id_uporabnik')));
				while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
					$stmt3 = $db->prepare("SELECT * FROM stanovanje_2 WHERE id =?");
					$stmt3->execute(array($row2['stanovanje_id']));
					$stanData = $stmt3->fetch(PDO::FETCH_ASSOC);
					$result .= '
					<div class="container-fluid">

						<!-- Stanovanje 1 primer -->
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading" style="max-height:130px;min-height:50px;">
									<h4 class="panel-title">
										<a data-toggle="collapse" href="#stanovanje1" class="h3">'.$stanData['ulica'].' '.$stanData['hisna_stevilka'].', <code>'.$stanData['postna_stevilka'].' '.$stanData['mesto'].'</code></a>
											<div class="btn-group" role="group" aria-label="..." style="float: right;">
                                            
                                            <a href="stanovanje_ideal_cimer.php?stan_id='.$stanData['id'].'" class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="left" title="Upravljanje z stanovalci"><i class="fa fa-user fa-2x"> </i></a>
                                            <a href="profil_stanovanja.php?stan_id='.$stanData['id'].'" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="left" title="Uređivanje podatki o stanovanju"><i class="fa fa-eye fa-2x"> </i></a>
                                            <a href="#" class="btn btn-xs btn-danger"  data-toggle="modal" data-target="#odjavaStanovanja" type="button"><i class="fa fa-sign-out fa-2x"> </i></a>
       
                                           
									<!-- Modal -->
									<div id="odjavaStanovanja" class="modal fade" role="dialog">
									  <div class="modal-dialog">
									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">Odjava stanovanja</h4>
									      </div>
									      <div class="modal-body">
										        <p>Ste prepričani, da želite odjaviti stanovanje?</p>
									      </div>
									      <div class="modal-footer">
									        <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="window.location.href=\''.$_baseURL.'process.php?lbrw='.$TOKEN.'&zbrw=3&stan_id='.$stanData['id'].'\'">Odjavi</button>
									        <button type="button" class="btn btn-default" data-dismiss="modal">Zapri</button>
									      </div>
									    </div>
									  </div>
									</div>		
				                                        <!-- modal konec-->
										</div>
									</h4>
								</div>
								<!--<div id="stanovanje1" class="panel-collapse collapse">
									<div class="panel-body">Informacije o stanovanju</div>
								</div>-->
							</div>
						</div>
						<!-- Stanovanje 1 konec -->
					</div>';
				}

		$result .= '
				</div>
			</div>
		</div>
	</div>



	</div>
	</div>
	<!-- USER PROFILE ROW END-->
	</div>
	<!-- CONATINER END -->';

		return $result;
		}
	}
?>
