<?php
class Profil {

		var $justView = 0;
		var $id;
		var $idCurrentUser;

		function __construct($idCurrentUser, $id = "") {
			$this->idCurrentUser = $idCurrentUser;
			if(($id != "") && ($id >= 0)) {
				$this->id = $id;
				$this->justView = 1;
			}
		}

		function __toString() {
			global $_baseURL;
			global $TOKEN;
			global $db;
			//global $user;
			if($this->justView == 1) {
				$temp_data = $db->prepare("SELECT * FROM uporabnik WHERE id_uporabnik =? LIMIT 1");
				$temp_data->execute(array($this->id));
				$temp_data = $temp_data->fetch(PDO::FETCH_ASSOC);
				$user = new User(encrypt($temp_data['mail']), encrypt($temp_data['geslo']));
			} else {
				$user = new User($_SESSION['mail'], $_SESSION['pass']);
			}
			$result .= '	
<body class="">

	<!-- NAVBAR CODE END -->
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h2>Meni</h2>

				<br> <br>

			</div>
		</div>
		<!-- USER PROFILE ROW STARTS-->

		<div class="row">
			<div class="col-md-3 col-sm-6">

				<div class="user-wrapper" style="padding-bottom: 20px;">
					<img src="'.$user->getData("slika").'" class="img-responsive">';

				if($this->justView == 0) {
					$result .= '
					<!-- Button trigger modal -->
					<button type="button" class="btn btn-default btn-group-justified"
						data-toggle="modal" data-target="#slikaProfila">
						<span class="fa fa-upload"></span> Naloži sliko
					</button>';
				}

				$result .= '
					<!-- Modal -->
					<div class="modal fade" id="slikaProfila" role="dialog">
						<div class="modal-dialog">

							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Naloži sliko</h4>
								</div>
								<div class="modal-body">

									<form action="'.$_baseURL.'process.php?lbrw='.$TOKEN.'&zbrw=7&id='.encrypt($user->getData("id_uporabnik")).'" name="formSlika" method="post">
										<input placeholder="URL slike" class="form-control" name="slikaUrl" type="text" required>
									</form>

								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-primary" onclick="document.formSlika.submit()">
										<div id="form0_submit"><i class="fa fa-save"></i> Shrani</div>
									</button>
									<button type="button" class="btn btn-default"
										data-dismiss="modal">Zapri</button>

								</div>
							</div>

						</div>
					</div>



					<div class="description">

						<h4 class="text-center">'.$user->getData("ime").' '.$user->getData("priimek").'</h4>

						<hr>
						<h5><b>Mesto:</b> '.$user->getData("mesto").'</h5>';
				$stmt_prov_cont = $db->prepare("SELECT * FROM prijavljeni WHERE stanovanje_id IN (SELECT id FROM stanovanje_2 WHERE uporabnik_id =?) AND uporabnik_id=?");
				$stmt_prov_cont->execute(array($this->idCurrentUser, $user->getData("id_uporabnik")));
				$stmt_prov_cont = $stmt_prov_cont->rowCount();
				if(($stmt_prov_cont > 0) || ($this->justView == 0)) {
					$result .= '
						<h5><b>Naslov:</b> '.$user->getData("adresa").'</h5>
						<h5><b>Tel. št.:</b> '.$user->getData("tel_st").'</h5>';
				}
					$result .= '
						<h5><b>E-mail:</b> <a href="mailto:'.$user->getData("mail").'">'.$user->getData("mail").'</a></h5>
						<!--  <a class="btn btn-block btn-social btn-facebook" onclick="_gaq.push([\'_trackEvent\', \'btn-social\', \'click\', \'btn-facebook\']);">
            <span class="fa fa-facebook"></span> Najdi me na facebooku
          </a> -->
';
				if($this->justView == 0) {
					$result .= '
						<br />
						<!-- Button trigger modal -->
						<button type="button" class="btn btn-default btn-group-justified" data-toggle="modal" data-target="#osnovniPodatki">
							<span class="fa fa-edit"></span> Urejanje
						</button>';
				}
					$result .= '
						<!-- Modal -->
						<div class="modal fade" id="osnovniPodatki" role="dialog">
							<div class="modal-dialog">

								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">Spreminjanje osnovnih podatkov</h4>
									</div>
									<div class="modal-body">
										<h3>Vpišite nove podatke</h3>
										<p style="padding: 5px">
										<form role="form" action="" method="post">


											<h5>Mesto:</h5>
											<div class="form-group">
												<label class="sr-only" for="mesto">Mesto</label>
													<input type="text" name="mesto" placeholder="npr. Maribor" class="form-control" id="mesto" value="'.$user->getData("mesto").'">
											</div>

											<h5>Naslov:</h5>
											<div class="form-group">
												<label class="sr-only" for="adresa">Adresa</label> <input
													type="text" name="adresa"
													placeholder="npr. Smetanova ulica 17" class="form-control"
													id="adresa" value="'.$user->getData("adresa").'">
											</div>

											<h5>Telefonska številka:</h5>
											<div class="form-group">
												<label class="sr-only" for="tel">Telefonska številka</label> <input
													type="text" name="tel" placeholder="npr. 041/090-1234"
													class="form-control" id="tel" value="'.$user->getData("tel_st").'">
											</div>

<!--											<h5>E-pošta:</h5>
											<div class="for
                                    m-group">
												<label class="sr-only" for="email">E-mail</label> <input
													type="text" name="email" placeholder="E-mail"
													class="form-control" id="email" value="'.$user->getData("mail").'">
											</div>-->


										</form>
										</p>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-primary" onclick="update_form(1)">
											<div id="form1_submit"><i class="fa fa-save"></i> Shrani</div>
										</button>
										<button type="button" class="btn btn-default"
											data-dismiss="modal">Zapri</button>

									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>

';
				if($this->justView == 0) {
					$result .= '
			<!-- PODMENI ZA POSTAVKE -->
			<div class="col-md-9 col-sm-6  user-wrapper">
				<ul class="nav nav-pills success">

					<li class="active"><a href="profil.php"><i class="fa fa-edit"></i> Urejanje profila</a></li>
					<li><a href="najdi_cimra.php"><i class="fa fa-group"></i> Idealni cimer</a></li>
					<li><a href="iskanje_stanovanja.php"><i class="fa fa-building"></i> Idealno stanovanje</a></li>
				</ul>
			</div>';
					$var_temp_padding = "20";
				} else { $var_temp_padding = "0";	}
			$result .= '
			<div class="col-md-9 col-sm-6  user-wrapper" style="padding-top: '.$var_temp_padding.'px">
				<div class="description">
					<i class="fa fa-question fa-2x"> Opis idealnega	cimra/stanovanja</i>

					<hr>
					<h5><b>Idealni cimer:</b> '.$user->getData("i_cimer").'</h5>
					<h5><b>Idealno stanovanje:</b> '.$user->getData("i_stan").'</h5>
					<hr>

					';
				if($this->justView == 0) {
					$result .= '<br />
					<!-- Button trigger modal -->
					<button type="button" class="btn btn-default btn-group-justified"
						data-toggle="modal" data-target="#idealniCimerStanovanje">
						<span class="fa fa-edit"></span> Uredi
					</button>';
				}

				$result .= '
					<!-- Modal -->
					<div class="modal fade" id="idealniCimerStanovanje" role="dialog">
						<div class="modal-dialog">

							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Spreminjanje podatkov</h4>
								</div>
								<div class="modal-body">
									<h3>Opis idealnega cimra/stanovanje</h3>
									<p style="padding: 5px">
									<form role="form" action="" method="post">


										<h5>Idealni cimer:</h5>
										<div class="form-group">
											<label class="sr-only" for="i_cimer">Idealni cimer</label> <input
												type="text" name="i_cimer" placeholder=""
												class="form-control" id="i_cimer" value="'.$user->getData("i_cimer").'">
										</div>

										<h5>Idealno stanovanje:</h5>
										<div class="form-group">
											<label class="sr-only" for="i_stan">Idealno stanovanje</label> <input
												type="text" name="i_stan" placeholder=""
												class="form-control" id="i_stan" value="'.$user->getData("i_stan").'">
										</div>


									</form>
									</p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-primary" onclick="update_form(2)">
										<div id="form2_submit"><i class="fa fa-save"></i> Shrani</div>
									</button>
									<button type="button" class="btn btn-default"
										data-dismiss="modal">Zapri</button>

								</div>
							</div>

						</div>
					</div>

				</div>

			</div>


			<div class="col-md-5 col-sm-12  user-wrapper"
				style="padding-top: 20px">
				<div class="description">
					<i class="fa fa-building fa-2x"> Izobrazba/Služba</i>

					<hr>
					<h5><b>Osnovna šola:</b> '.$user->getData("sola_os").'</h5>
					<h5><b>Srednja šola:</b> '.$user->getData("sola_sr").'</h5>
					<h5><b>Fakulteta:</b> '.$user->getData("sola_fc").'</h5>
					
					<h5><b>Delovno mesto:</b> '.$user->getData("delovno_mesto").'</h5>
					<h5><b>Izkušnje:</b> '.$user->getData("delovne_iskusnje").'</h5>

					<h5><b>Dodaten opis:</b> '.$user->getData("delo_opis").'</h5>
					<hr>';
				if($this->justView == 0) {
					$result .= '
					<br />
					<!-- Button trigger modal -->
					<button type="button" class="btn btn-default btn-group-justified"
						data-toggle="modal" data-target="#obrazovanjePodatki">
						<span class="fa fa-edit"></span> Uredi
					</button>';
				}
					$result .= '
					<!-- Modal -->
					<div class="modal fade" id="obrazovanjePodatki" role="dialog">
						<div class="modal-dialog">

							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Spreminjanje podatkov o
										izobrazbi</h4>
								</div>
								<div class="modal-body">
									<h3>Vpišite nove podatke</h3>
									<p style="padding: 5px">
									<form role="form" action="" method="post">


										<h5>Osnovna šola:</h5>
										<div class="form-group">
											<label class="sr-only" for="osnovna_sola">Osnovna
												sola</label> <input type="text" name="osnovna_sola"
												placeholder="npr. #OSMatijaGubca" class="form-control"
												id="osnovna_sola" value="'.$user->getData("sola_os").'">
										</div>

										<h5>Srednja šola:</h5>
										<div class="form-group">
											<label class="sr-only" for="srednja">Srednja sola</label> <input
												type="text" name="srednja"
												placeholder="npr. #SplosnaGimnazijaMaribor"
												class="form-control" id="srednja" value="'.$user->getData("sola_sr").'">
										</div>

										<h5>Fakultet/Visoka šola:</h5>
										<div class="form-group">
											<label class="sr-only" for="fakultet">Fakultet/Visoka
												šola</label> <input type="text" name="fakultet"
												placeholder="npr. #FERI"
												class="form-control" id="fakultet" value="'.$user->getData("sola_fc").'">
										</div>

										<h5>Delovno mesto:</h5>
										<div class="for
                                    m-group">
											<label class="sr-only" for="sluzba">Delovno mesto</label> <input
												type="text" name="sluzba" placeholder="npr. #Gorenje"
												class="form-control" id="sluzba" value="'.$user->getData("delovno_mesto").'">
										</div>

										<h5>Prejšnja delovna mesta:</h5>
										<div class="for
                                    m-group">
											<label class="sr-only" for="delo">Prejšnja delovna
												mesta</label> <input type="text" name="delo" placeholder="npr. #google #ingrad..."
												class="form-control" id="delo" value="'.$user->getData("delovne_iskusnje").'">
										</div>

										<h5>Dodaten opis:</h5>
										<div class="for
                                    m-group">
											<label class="sr-only" for="dod_opis">Prejšnja
												delovna mesta</label> <input type="text" name="dod_opis"
												placeholder="..." class="form-control" id="dod_opis" value="'.$user->getData("delo_opis").'">
										</div>

									</form>
									</p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-primary" onclick="update_form(3)">
										<div id="form3_submit"><i class="fa fa-save"></i> Shrani</div>
									</button>
									<button type="button" class="btn btn-default"
										data-dismiss="modal">Zapri</button>

								</div>
							</div>

						</div>
					</div>

				</div>

			</div>

			<div class="col-md-4 col-sm-12 user-wrapper"
				style="padding-top: 20px;">
				<div class="description">
					<i class="fa fa-info-circle fa-2x"> Interesi/O meni</i>

					<hr>

					<h5><b>Interesi:</b> '.$user->getData("interesi").'</h5>
					<h5><b>Hobiji:</b> '.$user->getData("hobiji").'</h5>
					<h5><b>Alergije:</b> '.$user->getData("alergije").'</h5>
					<h5><b>Hišni ljubljenčki:</b> '.$user->getData("ljubimci").'</h5>
					<h5><b>Zanimivosti o meni:</b> '.$user->getData("o_meni").'</h5>

					<hr>
					<h5><b>Pri ljudeh mi je všeč:</b> '.$user->getData("vsec_mi_je").'</h5>
';
				if($this->justView == 0) {
					$result .= '
					<br />
					<!-- Button trigger modal -->
					<button type="button" class="btn btn-default btn-group-justified"
						data-toggle="modal" data-target="#osebiPodatki">
						<span class="fa fa-edit"></span> Uredi
					</button>';
				}
					$result .= '
					<!-- Modal -->
					<div class="modal fade" id="osebiPodatki" role="dialog">
						<div class="modal-dialog">

							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Spreminjanje podatkov</h4>
								</div>
								<div class="modal-body">
									<h3></h3>
									<p style="padding: 5px">
									<form role="form" action="" method="post">


										<h5>Interesi:</h5>
										<div class="form-group">
											<label class="sr-only" for="interesi">Interesi</label> <input
												type="text" name="interesi" placeholder="npr. #sport #nabiranjeKamenckov #sashaGrey #voajerstvo"
												class="form-control" id="interesi" value="'.$user->getData("interesi").'">
										</div>

										<h5>Hobiji:</h5>
										<div class="form-group">
											<label class="sr-only" for="hobiji">Hobiji</label> <input
												type="text" name="hobiji" placeholder="npr. #planinarjenje #potapljanje #programiranje "
												class="form-control" id="hobiji" value="'.$user->getData("hobiji").'">
										</div>

										<h5>Alergije:</h5>
										<div class="form-group">
											<label class="sr-only" for="alergije">Alergije</label> <input
												type="text" name="alergije" placeholder="npr. #stupidPeople #cvetniPrah"
												class="form-control" id="alergije" value="'.$user->getData("alergije").'">
										</div>

										<h5>Hišni ljubljenčki:</h5>
										<div class="for
                                    m-group">
											<label class="sr-only" for="hisni_ljubljencki">Hišni
												ljubljenčki</label> <input type="text" name="hisni_ljubljencki"
												placeholder="npr. #zirafa #aligator #pes" class="form-control" id="hisni_ljubljencki" value="'.$user->getData("ljubimci").'">
										</div>

										<h5>Zanimivosti o meni:</h5>
										<div class="for
                                    m-group">
											<label class="sr-only" for="o_meni">Zanimivosti o
												meni</label> <input type="text" name="o_meni" placeholder="npr. #lep #muySimpatico #kralj"
												class="form-control" id="o_meni" value="'.$user->getData("o_meni").'">
										</div>

										<h5>Pri ljudjeh mi je všeč:</h5>
										<div class="for
                                    m-group">
											<label class="sr-only" for="pri_ljudjeh">Pri ljudjeh
												mi je všeč</label> <input type="text" name="pri_ljudjeh"
												placeholder="npr. #dobraOseba " class="form-control" id="pri_ljudjeh" value="'.$user->getData("vsec_mi_je").'">
										</div>

									</form>
									</p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-primary" onclick="update_form(4)">
										<div id="form4_submit"><i class="fa fa-save"></i> Shrani</div>
									</button>
									<button type="button" class="btn btn-default"
										data-dismiss="modal">Zapri</button>

								</div>
							</div>

						</div>
					</div>

				</div>

			</div>




		</div>

	</div>

';
if($this->justView == 0) {
$result .= '
	<script>

		function update_form(form) {
			switch(form) {
				case 1:
					$("#form1_submit").html("<div style=\"width: 172px\"><i class=\"fa fa-spinner fa-pulse\"></i></div>");
					var mesto = $("#mesto").val();
					var adresa = $("#adresa").val();
					var tel = $("#tel").val();
					update("'.encrypt("uporabnik").'", "'.encrypt("mesto").'", mesto, "'.encrypt($user->getID()).'");
					update("'.encrypt("uporabnik").'", "'.encrypt("adresa").'", adresa, "'.encrypt($user->getID()).'");
					var res = update("'.encrypt("uporabnik").'", "'.encrypt("tel_st").'", tel, "'.encrypt($user->getID()).'");
					if(res == 1) {	window.location.href = "'.$_baseURL.'profil.php";	}
				break;
				case 2:
					$("#form2_submit").html("<div style=\"width: 172px\"><i class=\"fa fa-spinner fa-pulse\"></i></div>");
					var icimer = $("#i_cimer").val();
					var istan = $("#i_stan").val();
					update("'.encrypt("uporabnik").'", "'.encrypt("i_cimer").'", icimer, "'.encrypt($user->getID()).'");
					var res = update("'.encrypt("uporabnik").'", "'.encrypt("i_stan").'", istan, "'.encrypt($user->getID()).'");
					if(res == 1) {	window.location.href = "'.$_baseURL.'profil.php";	}
				break;
				case 3:
					$("#form3_submit").html("<div style=\"width: 172px\"><i class=\"fa fa-spinner fa-pulse\"></i></div>");
					var os1 = $("#osnovna_sola").val();
					var sr1 = $("#srednja").val();
					var fc1 = $("#fakultet").val();
					var sl1 = $("#sluzba").val();
					var dl1 = $("#delo").val();
					var dod1 = $("#dod_opis").val();
					update("'.encrypt("uporabnik").'", "'.encrypt("sola_os").'", os1, "'.encrypt($user->getID()).'");
					update("'.encrypt("uporabnik").'", "'.encrypt("sola_sr").'", sr1, "'.encrypt($user->getID()).'");
					update("'.encrypt("uporabnik").'", "'.encrypt("sola_fc").'", fc1, "'.encrypt($user->getID()).'");
					update("'.encrypt("uporabnik").'", "'.encrypt("delovno_mesto").'", sl1, "'.encrypt($user->getID()).'");
					update("'.encrypt("uporabnik").'", "'.encrypt("delovne_iskusnje").'", dl1, "'.encrypt($user->getID()).'");
					var res = update("'.encrypt("uporabnik").'", "'.encrypt("delo_opis").'", dod1, "'.encrypt($user->getID()).'");
					if(res == 1) {	window.location.href = "'.$_baseURL.'profil.php";	}
				break;
				//TODO jos za 4, i ove gore koje fale
				case 4:
					$("#form4_submit").html("<div style=\"width: 172px\"><i class=\"fa fa-spinner fa-pulse\"></i></div>");
					var int = $("#interesi").val();
					var hob = $("#hobiji").val();
					var ale = $("#alergije").val();
					var lju = $("#hisni_ljubljencki").val();
					var abo = $("#o_meni").val();
					var plj = $("#pri_ljudjeh").val();
					update("'.encrypt("uporabnik").'", "'.encrypt("interesi").'", int, "'.encrypt($user->getID()).'");
					update("'.encrypt("uporabnik").'", "'.encrypt("hobiji").'", hob, "'.encrypt($user->getID()).'");
					update("'.encrypt("uporabnik").'", "'.encrypt("alergije").'", ale, "'.encrypt($user->getID()).'");
					update("'.encrypt("uporabnik").'", "'.encrypt("ljubimci").'", lju, "'.encrypt($user->getID()).'");
					update("'.encrypt("uporabnik").'", "'.encrypt("o_meni").'", abo, "'.encrypt($user->getID()).'");
					var res = update("'.encrypt("uporabnik").'", "'.encrypt("vsec_mi_je").'", plj, "'.encrypt($user->getID()).'");
					if(res == 1) {	window.location.href = "'.$_baseURL.'profil.php";	}
				break;
			}
		}
		function update(tab, column, data, user_id) {	
				
			var result = 0;

			var pod0 = 1;	
			var pod1 = tab;
			var pod2 = column;
			var pod3 = data;
			var pod4 = user_id;
			var pod5 = "'.encrypt("id_uporabnik").'";

			$.ajax({
				type: "POST",
				url: "'.$_baseURL.'ajax.php?lbrw='.encrypt($TOKEN).'",
				async: false,
				data: { zbrw: pod0, table: pod1, column: pod2, data: pod3, uid: pod4, uidc: pod5 },
				success: function(data) {
				//	alert(data);   
					result = 1;
				},
				error: function(data) {
					console.log("Greska kod spremanja atributa. [" + column + "]");
				}
			});
			return result;
		}
		</script>';
}
$result .= '
	<!-- USER PROFILE ROW END-->
	</div>
	<!-- CONATINER END -->

	<!-- REQUIRED SCRIPTS FILES -->
	<!--<script data-rocketsrc="http://www.designbootstrap.com/track/ga.js"
		type="text/rocketscript" data-rocketoptimized="true"></script>-->
	<!-- CORE JQUERY FILE -->
	<script data-rocketsrc="assets/js/jquery-1.11.1.js"
		type="text/rocketscript" data-rocketoptimized="true"></script>
	<!-- REQUIRED BOOTSTRAP SCRIPTS -->
	<script data-rocketsrc="assets/js/bootstrap.js"
		type="text/rocketscript" data-rocketoptimized="true"></script>

</body>';
			return $result;
		}
	}

?>
