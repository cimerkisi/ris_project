<?php

	class Menu {

		var $menu_link_1 = "logout.php";
		var $menu_link_1_str = "Odjava";
		var $menu_link_2 = "profil_uporabnika.php";
		var $menu_link_2_str = "Urejanje profila";
		var $menu_link_3 = "pregled_dodanih_stanovanj.php";
		var $menu_link_3_str = "Pregled dodanih stanovanj";
		var $menu_link_4="dodajanje_stanovanja.php";
		var $menu_link_4_str="Dodaj stanovanje";
		var $menu_pozdrav="Pozdravljen, uporabnik";


		function __construct($username, $password) {
			global $user;
			if(!($username == "")) {
				$this->menu_link_1 = "idealna_stanovanja.php";
				$this->menu_link_1_str = "Idealna stanovanja";
				$this->menu_link_odjava= "logout.php";
				$this->menu_link_odjava_str = "ODJAVA";
				$this->menu_link_3 = "pregled_dodanih_stanovanj.php";
				$this->menu_link_3_str = "Pregled stanovanj";
				$this->menu_link_4="dodajanje_stanovanja.php";
				$this->menu_link_4_str="Dodaj stanovanje";

				$this->menu_pozdrav = "Pozdravljen, " . ($user->getData("ime"));
				$this->menu_pozdrav_link="profil.php";
			}
		}
		
		function __toString() {
			global $_baseURL;
			return '
    			<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
		        <div class="container-fluid">
	                <div class="navbar-header">
		                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		                    <span class="sr-only">Toggle navigation</span>
	        	            <span class="icon-bar"></span>
	                	    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>
	        	        <a class="navbar-brand page-scroll" href="'.$_baseURL.'profil.php"><img src="'.$_baseURL.'src/img/logo5.png" class="img-responsive"></a>
			</div>
		            <!-- Collect the nav links, forms, and other content for toggling -->
		            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		                <ul class="nav navbar-nav navbar-right">
						

		                	 <li>
		                        <a href="'.$this->menu_pozdrav_link.'">'.$this->menu_pozdrav.'</a>
		                    </li>

		                    <li>
		                        <a href="'.$_baseURL.$this->menu_link_1.'">'.$this->menu_link_1_str.'</a>
		                    </li>
		                   
		                    <li>
		                        <a href="'.$_baseURL.$this->menu_link_4.'">'.$this->menu_link_4_str.'</a>
		                    </li>

		                    <li>
		                        <a href="'.$_baseURL.$this->menu_link_3.'">'.$this->menu_link_3_str.'</a>
		                    </li>

		                     <li>
		                        <a href="'.$_baseURL.$this->menu_link_odjava.'">'.$this->menu_link_odjava_str.'</a>
		                    </li>
		                    
		                </ul>
		            </div>
		            <!-- /.navbar-collapse -->
		        </div>
		        <!-- /.container-fluid -->
			</nav>';
		}

	}

?>
