<?php

//	class Dodajanje_stanovanja extends MainView {
	class Dodajanje_stanovanja {

		function __construct() {}

		function __toString() {
			global $db;
			$result = '			
        <body class="">

	<!-- NAVBAR CODE END -->
	<div class="container">
		<div class="row">
			<div class="text-center">
				<h2>Meni</h2>

				<br> <br>

			</div>
		</div>

		<!-- USER PROFILE ROW STARTS-->
		<div class="row">

			<div class="col-md-12 col-sm-12  user-wrapper"
				style="padding-bottom: 10px;">
				<div class="description">
					<div class="container-fluid text-center">
						<i class="fa fa-building fa-2x"> </i>
						<t class="text-center h2"> Dodajanje stanovanja </t>
						<hr>

						<br />


					</div>



					<div class="container-fluid">
<form role="form" action="dod_stan_pro.php" method="post">	
<div class="text_stanovanje">
                            	Naslov
                        		</div>
                                	<br />	
                                <div class="form-group col-md-8 col-lg-8 col-sm-12">
			                    		<label class="sr-only" for="ulica">Ulica</label>
			                        	<input type="text" name="ulica" placeholder="Ulica" class="form-control" id="ulica" required>
			                        </div>
			                        <div class="form-group col-md-4 col-lg-4 col-sm-12">
			                        	<label class="sr-only" for="hisna_stevilka">Hišna Številka</label>
			                        	<input type="text" name="hisna_stevilka" placeholder="Hišna številka" class="form-control" id="hisna_stevilka" required>
			                        </div>
                                    
                                    
                                    <div class="form-group col-md-8 col-lg-8 col-sm-12">
			                    		<label class="sr-only" for="mesto">Mesto</label>
			                        	<input type="text" name="mesto" placeholder="Mesto" class="form-control" id="mesto" required>
			                        </div>
			                        <div class="form-group col-md-4 col-lg-4 col-sm-12">
			                        	<label class="sr-only" for="postna_stevilka">Poštna številka</label>
			                        	<input type="text" name="postna_stevilka" placeholder="Poštna številka" class="form-control" id="postna_stevilka" required>
			                        </div>
                                    
                                     
                                 
                                <div class="text_stanovanje">
                            	Število sob / št. postelj na sobo / št. prostih mest
                        	</div>
                                 <br />
                                <div class="btn-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                <label class="sr-only" for="stevilo_sob">Število sob</label>
                                <select name="br_soba" class="form-control btn btn-default">
                                <option value="5">5-sobno</option>
                                <option value="4">4-sobno</option>
                                <option value="3">3-sobno</option>
  				<option value="2">2-sobno</option>
 				<option value="1" selected>1-sobno</option>
 								
                                </select>
			                      
			                        </div>
                                  
                                <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                <label class="sr-only" for="stevilo_sob">Število postelj na sobo</label>
                                <select name="br_kreveta" class="form-control btn btn-default">
	                                <option value="5">5-posteljna</option>
	                                <option value="4">4-posteljna</option>
	                                <option value="3">3-posteljna</option>
	  				<option value="2">2-posteljna</option>
	 				<option value="1" selected>1-posteljna</option>						
                                </select>
			                      
			                        </div>
                                    
                                     <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                <label class="sr-only" for="stevilo_sob">Število prostih mest</label>
                                <select name="br_mesta" class="form-control btn btn-default">
                                <option value="5">5-mest</option>
                                <option value="4">4-mesta</option>
                                <option value="3">3-mesta</option>
  				<option value="2">2-mesti</option>
 				<option value="1" selected>1-mesto</option>
 								
                                </select>
			                      
			                        </div>
                                    
                                    </div>
                                     <br />
                                     <div class="text_stanovanje">
                            	Velikost stanovanja
                                </div>
                                     <br />
                                    <div class="input-group col-lg-10 col-md-12 col-sm-12 col-xs-12 col-lg-push-1">
  <input type="text" name="velikost_stanovanja" class="form-control" placeholder="Velikost stanovanja" aria-describedby="basic-addon2" required>
  <span class="input-group-addon" id="basic-addon2">m&sup2;</span>
</div>

                                     <br />
                                     <div class="text_stanovanje">
                            	Dodatki stanovanju (ustrezno označite)
                                </div>
                                 <br />
                                
                               
                                  <div class="btn-group">';
                                


				$stmt = $db->prepare("DESCRIBE kriterij");
				$stmt->execute();
				
				while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {

					$temp_str = explode("_", $row['Field']);
					if($temp_str[0] == "stan") {
						$str_main = str_replace("stan_", "", $row['Field']);
						$str_main_temp = str_replace("_", " ", $str_main);
						$str_main_temp = ucfirst($str_main_temp);
						$result .= '<div class="form-group col-lg-4 col-md-6 col-xs-6">
								<div class="checkbox">
							           <label>
							             <input type="checkbox" name="'.$str_main.'" id="'.$str_main.'"/>
							               '.$str_main_temp.'
								   </label>
								</div>
							    </div>';
					}
                                    
                                }
                                    
                                    
                                    
                                    
                                    
                               $result .= '</div>





                                     <br />
                                <div class="text_stanovanje">     
                            	Dodaten opis
                        	</div>
                                <br />
                                <div class="col-lg-12 col-md-12 col-xs-12" >
			                        	<label class="sr-only" for="opis">Opis</label>
			                        	<textarea type="text" name="opis_stanovanja" class="form-control" id="opis" style="height:70px"></textarea>
			                        </div>
                                <div class="text_stanovanje" style="margin-top:20px">     
                            	Stroški
                        	</div>
                                <br />
                                <div class="col-xs-3" >
					<label>Najemnina</label>
                                    <div class="input-group">
					  <span class="input-group-addon">€</span>
					  <input type="text" name="stroski_najamnine" class="form-control" aria-label="Amount (to the nearest dollar)" required>
					  <span class="input-group-addon">.00</span>
				    </div>
			        </div>
                                <div class="col-xs-3" >
					<label>Obratovalni stroški</label>
                                    <div class="input-group">
					  <span class="input-group-addon">€</span>
					  <input type="text" name="stroski_obratovalni" id="stroski_fiksni" class="form-control" aria-label="Amount (to the nearest dollar)" required>
					  <span class="input-group-addon">.00</span>
				    </div>
			        </div>
                                <div class="col-xs-3" >
					<label>Vrsta</label>
                                    <div class="input-group">
					<select name="stroski_vrsta" class="form-control btn btn-default" id="stroski">
		                                <option value="fiksni" selected>Fiksni</option>
        		                        <option value="spremenjivi">Spremenljivi</option>	
	                                </select>
				    </div>
			        </div>
				<script>
					$("#stroski").change(function(){
						var stat = $("#stroski").val();
						if(stat == "fiksni") {
							$("#stroski_fiksni").attr("disabled", false);	
						} else {
							$("#stroski_fiksni").attr("disabled", true);
						}
					});
				</script>
                                <div class="col-xs-3" >
					<label>Varščina</label>
                                    <div class="input-group">
					  <span class="input-group-addon">€</span>
					  <input type="text" name="stroski_varscina" class="form-control" aria-label="Amount (to the nearest dollar)" required>
					  <span class="input-group-addon">.00</span>
				    </div>
			        </div>
                                  <div class="btn-group col-lg-12" style="margin-top:20px">			
               <button class="btn btn-primary" type="submit" class="btn btn-primary" style="float:right">Shrani</button>
                                    </div>
                                    </div>
			             </form>
					</div>
				</div>
			</div>

		</div>

	</div>



	</div>
	</div>
	<!-- USER PROFILE ROW END-->
	</div>
	<!-- CONATINER END -->



	<!-- REQUIRED SCRIPTS FILES -->
	<script data-rocketsrc="http://www.designbootstrap.com/track/ga.js"
		type="text/rocketscript" data-rocketoptimized="true"></script>
	<!-- CORE JQUERY FILE -->
	<script data-rocketsrc="assets/js/jquery-1.11.1.js"
		type="text/rocketscript" data-rocketoptimized="true"></script>
	<!-- REQUIRED BOOTSTRAP SCRIPTS -->
	<script data-rocketsrc="assets/js/bootstrap.js"
		type="text/rocketscript" data-rocketoptimized="true"></script>

</body>
			';

			return $result;
		}
	
	}

?>
