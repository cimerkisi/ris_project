<?php
class Iskanje_stanovanja {

		function __construct() {}

		function __toString() {
			global $_baseURL;
			global $TOKEN;
			global $user;
			global $db;
			$result = '



<body class="">

	<!-- NAVBAR CODE END -->
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h2>Meni</h2>

				<br> <br>

			</div>
		</div>
		<!-- USER PROFILE ROW STARTS-->
		<div class="row">


			<!-- PODMENI ZA POSTAVKE -->
			<div class="col-md-12 col-sm-12 user-wrapper"
				style="padding-bottom: 24px">

				<ul class="nav nav-pills success">

					<li><a href="profil.php"><i class="fa fa-edit"></i> Urejanje profila</a></li>
					<li><a href="najdi_cimra.php"><i class="fa fa-group"></i> Idealni cimer</a></li>
					<li class="active"><a href="iskanje_stanovanja.php"><i class="fa fa-building"></i> Idealno stanovanje</a></li>
				</ul>


			</div>

			<div class="col-md-12 col-sm-12  user-wrapper"
				style="padding-bottom: 20px; color: white">
				<div class="description" style="background-color: #764576;">


					<div class="container-fluid">
						<i class="fa fa-building fa-3x"> </i>
						<t class="text-center h3" style="color:white"> Moje idealno stanovanje </t>
						<hr>

						<form name="form1" method="POST" action="'.$_baseURL.'process.php?lbrw='.$TOKEN.'&zbrw=2&type='.encrypt("stan").'">

							<div>
								<h3>Mesto</h3>
								<div class="form-group">
									<input name="mesto" placeholder="Maribor" class="form-control" id="mesto" value="'.$user->getDataKriterijOstalo('mesto').'" type="text">
								</div>
							</div>

							<div class="form-top">
								<div class="form-top-left">
									<h3>Število sob / št. postelj na sobo</h3>
								</div>
							</div>
							<br />';

						$selected_1_1 = "";
						$selected_1_2 = "";
						$selected_1_3 = "";
						$selected_1_4 = "";
						$selected_1_5 = "";
						switch($user->getDataKriterijOstalo('st_sob')) {
							case 1:
								$selected_1_1 = "selected";
							break;
							case 2:
								$selected_1_2 = "selected";
							break;
							case 3:
								$selected_1_3 = "selected";
							break;
							case 4:
								$selected_1_4 = "selected";
							break;
							case 5:
								$selected_1_5 = "selected";
							break;
						}
						$selected_2_1 = "";
						$selected_2_2 = "";
						$selected_2_3 = "";
						$selected_2_4 = "";
						$selected_2_5 = "";
						switch($user->getDataKriterijOstalo('st_postelj')) {
							case 1:
								$selected_2_1 = "selected";
							break;
							case 2:
								$selected_2_2 = "selected";
							break;
							case 3:
								$selected_2_3 = "selected";
							break;
							case 4:
								$selected_2_4 = "selected";
							break;
							case 5:
								$selected_2_5 = "selected";
							break;
						}

						$result .= '
							<div class="btn-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12">
									<label class="sr-only" for="stevilo_sob">Število sob</label>
									<select class="form-control signup-input" name="st_sob" style="height: 50px; padding: 10px 10px; background-color: #FFFFFF">
										<option value="5" '.$selected_1_5.'>5-sobno</option>
										<option value="4" '.$selected_1_4.'>4-sobno</option>
										<option value="3" '.$selected_1_3.'>3-sobno</option>
										<option value="2" '.$selected_1_2.'>2-sobno</option>
										<option value="1" '.$selected_1_1.'>1-sobno</option>
									</select>

								</div>

								<div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12">
									<label class="sr-only" for="stevilo_sob">Število postelj na sobo</label>
									<select class="form-control signup-input" name="st_postelj" style="height: 50px; padding: 10px 10px; background-color: #FFFFFF">
										<option value="5" '.$selected_2_5.'>5-posteljna</option>
										<option value="4" '.$selected_2_4.'>4-posteljna</option>
										<option value="3" '.$selected_2_3.'>3-posteljna</option>
										<option value="2" '.$selected_2_2.'>2-posteljna</option>
										<option value="1" '.$selected_2_1.'>1-posteljna</option>

									</select>

								</div>
							</div>
							<div class="form-top">
								<div class="form-top-left">
									<h3>Stroški</h3>
								</div>
							</div>
							<br />
							<div class="btn-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12">
				                                <div class="col-xs-12" >
									<label>Max. na mesec</label>
					                                    <div class="input-group">
										  <span class="input-group-addon">€</span>
										  <input type="text" name="stroski_max" id="stroski_fiksni" value="'.$user->getDataKriterijOstalo('stroski_max').'" class="form-control" aria-label="Amount (to the nearest dollar)">
										  <span class="input-group-addon">.00</span>
									    </div>
								        </div>

								</div>
								<div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12">
				                                <div class="col-xs-12" >
									<label>Max. varščina</label>
					                                    <div class="input-group">
										  <span class="input-group-addon">€</span>
										  <input type="text" name="stroski_varscina" value="'.$user->getDataKriterijOstalo('stroski_varscina').'" id="stroski_fiksni" class="form-control" aria-label="Amount (to the nearest dollar)">
										  <span class="input-group-addon">.00</span>
									    </div>
								        </div>

								</div>

							</div>



							<div class="form-top">
								<div class="form-top-left">
									<h3>Velikost stanovanja</h3>
								</div>
							</div>
							<br />
							<div class="btn-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12">
				                                <div class="col-xs-12" >
				                                    <div class="input-group col-lg-10 col-md-12 col-sm-12 col-xs-12 col-lg-push-1">
									  <input type="text" name="velikost_stanovanja" class="form-control" value="'.$user->getDataKriterijOstalo('velikost_stanovanja').'" aria-describedby="basic-addon2">
									  <span class="input-group-addon" id="basic-addon2">m&sup2;</span>
								    </div>
								</div>

								</div>
							</div>

							<div class="form-top">
								<div class="form-top-left">
									<h3>Lastnosti</h3>
								</div>
							</div>

							<div class="btn-group col-lg-12 col-md-12 col-sm-12 col-xs-12">';

								$stmt = $db->prepare("DESCRIBE kriterij");
								$stmt->execute();
								
								while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
									$temp_str = explode("_", $row['Field']);
									if($temp_str[0] == "stan") {
										$str_main = str_replace("stan_", "", $row['Field']);
										$str_main_temp = str_replace("_", " ", $str_main);
										$str_main_temp = ucfirst($str_main_temp);
										
										$checked = "";
										if($user->getDataKriterij("stan_".$str_main) == 1) {
											$checked = "checked";
										}

										$result .= '<div class="form-group col-lg-4 col-md-6 col-xs-6">
												<div class="checkbox">
											           <label>
											             <input type="checkbox" name="'.$str_main.'" id="'.$str_main.'" '.$checked.'/>
											               '.$str_main_temp.'
											           </label>
											        </div>
											     </div>';
									}
                				                }







$result .= '
							</div>



							<div class="btn-group col-lg-12 col-md-12 col-sm-12 col-xs-12"
								style="height: 100px; padding: 30px 50px">
								<button type="button" class="btn btn-success" style="float: right" onclick="document.form1.submit()">
									<i class="fa fa-save"></i> Shrani
								</button>

							</div>
						</form>


						<br />

					</div>
				</div>

			</div>

		</div>

	</div>



	</div>



	<!-- USER PROFILE ROW END-->
	</div>
	<!-- CONATINER END -->



	<!-- REQUIRED SCRIPTS FILES -->
	<script data-rocketsrc="http://www.designbootstrap.com/track/ga.js"
		type="text/rocketscript" data-rocketoptimized="true"></script>
	<!-- CORE JQUERY FILE -->
	<script data-rocketsrc="assets/js/jquery-1.11.1.js"
		type="text/rocketscript" data-rocketoptimized="true"></script>
	<!-- REQUIRED BOOTSTRAP SCRIPTS -->
	<script data-rocketsrc="assets/js/bootstrap.js"
		type="text/rocketscript" data-rocketoptimized="true"></script>

</body>
';
			return $result;
		}
	}
?>
