<?php
class Profil_stanovanja{
		var $id;
		var $data;
		var $kriterij;
		var $myStan = 1;
		var $prijava = 0;

		function __construct($id) {
			$this->id = $id;
			$this->fetchData();
			$this->fetchDataKriterij();
			$this->checkIfStanMy();
			$this->checkIfPrijavljen();
		}

		function fetchData() {
			global $db;
			$stmt = $db->prepare("SELECT * FROM stanovanje_2 WHERE id=? LIMIT 1");
			$stmt->execute(array($this->id));
			$this->data = $stmt->fetch(PDO::FETCH_ASSOC);
		}

		function fetchDataKriterij() {
			global $db;
			$stmt = $db->prepare("SELECT * FROM kriterij WHERE sOc_ID=? AND sOc=? LIMIT 1");
			$stmt->execute(array($this->id, "stan"));
			$this->kriterij = $stmt->fetch(PDO::FETCH_ASSOC);
		}

		function getData($name) {
			return $this->data[$name];
		}

		function getDataKriterij($name) {
			return $this->kriterij[$name];
		}

		function checkIfStanMy() {
			global $user;
			global $db;

			$stmt = $db->prepare("SELECT * FROM stanovanje_2 WHERE id=? AND uporabnik_id=?");
			$stmt->execute(array($this->id, $user->getData('id_uporabnik')));
			$broji = $stmt->rowCount();
			if($broji == 0) {
				$this->myStan = 0;
			} else {
				$this->myStan = 1;
			}
		}

		function checkIfPrijavljen() {
			global $user;
			global $db;

			$stmt = $db->prepare("SELECT * FROM prijavljeni WHERE stanovanje_id=? AND uporabnik_id=?");
			$stmt->execute(array($this->id, $user->getData('id_uporabnik')));
			$broji = $stmt->rowCount();
			if($broji == 0) {
				$this->prijava = 0;
			} else {
				$this->prijava = 1;
			}
		}

		function __toString() {
			global $_baseURL;
			global $TOKEN;
			global $user;
			global $db;

			$gsi1 = "edit";
			$gsi2 = "Uredi podatke o stanovanju";
			$gsi3 = "Prijavljene in potrjene osebe";
			if($this->myStan == 0) {
				$gsi1 = "info";
				$gsi2 = "Podatki o stanovanju";
				$gsi3 = "Cimri";
			}
			$result = '
<body class="">

	<!-- NAVBAR CODE END -->
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h2>Meni</h2>

				<br> <br>

			</div>
		</div>
		<!-- USER PROFILE ROW STARTS-->
		<div class="row">
        			<!-- PODMENI ZA POSTAVKE -->
			<div class="col-md-12 col-sm-6  user-wrapper">

				<ul class="nav nav-pills success">
                
                
					<li class="active"><a href="profil_stanovanja.php?stan_id='.$this->id.'"><i class="fa fa-'.$gsi1.'"></i> '.$gsi2.'</a></li>
                    <li><a href="stanovanje_slike.php?stan_id='.$this->id.'"><i class="fa fa-image"></i> Fotogalerija</a></li>
					<li><a href="stanovanje_ideal_cimer.php?stan_id='.$this->id.'"><i class="fa fa-users"></i>
							'.$gsi3.'</a></li>';
				if($this->myStan == 0) {
					if($this->prijava == 1) {
						$result .= '<li class="active" id="prijavaLI"><a href="javascript::void(0)" onclick="prijavaStan(0)" id="prijavaButton" style="background-color: green"><i class="fa fa-check"></i> Prijavljen</a></li>';
					} else {
						$result .= '<li class="active" id="prijavaLI"><a href="javascript::void(0)" onclick="prijavaStan(1)" id="prijavaButton" style="background-color: orange"><i class="fa fa-sign-in"></i> Prijavi se</a></li>';
					}

					$result .= '
						<script>
							function prijavaStan(status) {
								$("#prijavaButton").html("<div style=\"width: 85px; padding-left: 35px\"><i class=\"fa fa-spinner fa-pulse\"></i></div>");
								var data0 = 2;
								var data1 = "'.encrypt($this->getData('id')).'";
								var data2 = "'.encrypt($user->getData('id_uporabnik')).'";
								
								$.ajax({
									type: "POST",
									url: "'.$_baseURL.'ajax.php?lbrw='.encrypt($TOKEN).'",
									async: false,
									data: { zbrw: data0, kbrw: status, pod1: data1, pod2: data2 },
									success: function(data) {
										$("#prijavaLI").html(data);  
										result = 1;
									},
									error: function(data) {
										console.log("Greska");
									}
								});
							}
						</script>
					';
					
				}

		$slika = $db->prepare("SELECT * FROM stanovanje_2_slike WHERE type=? AND stanovanje_id=? LIMIT 1");
		$slika->execute(array("main", $this->id));
		$slika = $slika->fetch(PDO::FETCH_ASSOC);
		$slika = $slika['src'];

$result .= '				</ul>


			</div>
            
			<div class="col-md-4 col-sm-6">

				<div class="user-wrapper" style="padding-bottom: 20px;">
					<img src="'.$slika.'" class="img-responsive" style="padding-top:20px;">

					<!-- Button trigger modal -->';

					$result .= '

					
            </div>
            </div>
            



            <div class="col-lg-8 col-sm-12  user-wrapper"
				style="padding-top: 20px">
				<div class="description">
					<i class="fa fa-euro fa-2x"> Stroški stanovanja</i>

					<hr>';
						$obratovalni_str_temp = "SPREMENLJIVI";
						if($this->getData('vrsta_stroskov') == "fiksni") {
							$obratovalni_str_temp = $this->getData('obratovalni_stroski') . " €";						
						}

					$result .= '
					<h5><b>Stroški najemnine:</b> '.$this->getData('najemnina').' €</h5>
					<h5><b>Obratovalni stroški:</b> '.$obratovalni_str_temp.'</h5>
					<h5><b>Varščina:</b> '.$this->getData('varscina').' €</h5>

					<br />
					<!-- Button trigger modal -->';

					if($this->myStan == 1) {
						$result .= '
						<button type="button" class="btn btn-default btn-group-justified"
							data-toggle="modal" data-target="#stroskiStanovanja">
							<span class="fa fa-edit"></span> Uredi podatke
						</button>';
					}

					$result .= '
					<!-- Modal -->
					<div class="modal fade" id="stroskiStanovanja" role="dialog">
						<div class="modal-dialog">

							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Spreminjanje podatkov o stroških stanovanja</h4>
								</div>
								<div class="modal-body">
									<p style="padding: 5px">
				<form role="form" action="" method="post" >			
                                <div class="text_stanovanje" style="margin-top:20px">     
                            	Stroški
                        	</div>
                                <br />
                                <div class="col-xs-12" >
					<label>Najemnina</label>
                                    <div class="input-group">
					  <span class="input-group-addon">€</span>
					  <input type="text" id="stroski_najamnine" value="'.$this->getData('najemnina').'" class="form-control" aria-label="Amount (to the nearest dollar)" required>
					  <span class="input-group-addon">.00</span>
				    </div>
			        </div>';
					$selected_1 = "";
					$selected_2 = "";
					$disabled_2X = "";
					switch($this->getData('vrsta_stroskov')) {
						case "fiksni":
							$selected_1 = "selected";
							$selected_2 = "";
						break;
						case "spremenjivi":
							$selected_1 = "";
							$selected_2 = "selected";
							$disabled_2X = ' disabled="disabled"';
						break;
					}
				$result .= '
                                <div class="col-xs-6" >
					<label>Obratovalni stroški</label>
                                    <div class="input-group">
					  <span class="input-group-addon">€</span>
					  <input type="text" id="stroski_obratovalni" value="'.$this->getData('obratovalni_stroski').'"'.$disabled_2X.' class="form-control" aria-label="Amount (to the nearest dollar)" required>
					  <span class="input-group-addon">.00</span>
				    </div>
			        </div>
                                <div class="col-xs-6" >
					<label>Vrsta</label>


                                    <div class="input-group">
					<select name="stroski_vrsta" class="form-control btn btn-default" id="stroski">
		                                <option value="fiksni" '.$selected_1.'>Fiksni</option>
        		                        <option value="spremenjivi" '.$selected_2.'>Spremenljivi</option>	
	                                </select>
				    </div>
			        </div>
				<script>
					$("#stroski").change(function(){
						var stat = $("#stroski").val();
						if(stat == "fiksni") {
							$("#stroski_obratovalni").attr("disabled", false);	
						} else {
							$("#stroski_obratovalni").attr("disabled", true);
						}
					});
				</script>
                                <div class="col-xs-12" >
					<label>Varščina</label>
                                    <div class="input-group">
					  <span class="input-group-addon">€</span>
					  <input type="text" id="stroski_varscina" value="'.$this->getData('varscina').'" class="form-control" aria-label="Amount (to the nearest dollar)" required>
					  <span class="input-group-addon">.00</span>
				    </div>
			        </div>
                               
									</p>
                                   </div>

              <div class="modal-footer" style="margin-top:260px;">
              <button type="button" class="btn btn-primary" onclick="update_form(2)">
              <div id="form2_submit"><i class="fa fa-save"></i> Shrani</div></button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Zapri</button>
            </div> 
            </div> 
			                    </form>

						</div>
					</div>

				</div>

			</div>


			<div class="col-md-12 col-sm-12  user-wrapper">
				<div class="description">
					<i class="fa fa-info fa-2x"> Splošni podatki o stanovanju</i>

					<hr>
					<h5><b>Naslov:</b> '.$this->getData('ulica').' '.$this->getData('hisna_stevilka').'</h5>
					<h5><b>Mesto:</b> '.$this->getData('mesto').', '.$this->getData('postna_stevilka').'</h5>
          			        <h5><b>Št. sob:</b> '.$this->getData('stevilo_sob').'</h5>
             		 	        <h5><b>Št. postelj na sobo:</b> '.$this->getData('stevilo_postelj').'</h5>
               				<h5><b>Št. prostih mest:</b> '.$this->getData('stevilo_prostih_mest').'</h5>
					<h5><b>Velikost stanovanja:</b> '.$this->getData('velikost_stanovanja').'</h5>
                    			<h5><b>Opis:</b> '.$this->getData('opis').'</h5>
                    			<h5><b>Ostalo: </b> ';


						$stmt = $db->prepare("DESCRIBE kriterij");
						$stmt->execute();
						
						while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		
							$temp_str = explode("_", $row['Field']);
							if($temp_str[0] == "stan") {
								$str_main = str_replace("stan_", "", $row['Field']);
								$str_main_temp = str_replace("_", " ", $str_main);
								$str_main_temp = ucfirst($str_main_temp);
		
								if($this->getDataKriterij("stan_".$str_main) == 1) {
									$result .= "<code>".$str_main_temp."</code>&nbsp;";
								}
							}
		                                    
		                                }


					$result .= '
					<br />
					<br>
					<!-- Button trigger modal -->';
					if($this->myStan == 1) {
						$result .= '
						<button type="button" class="btn btn-default btn-group-justified"
							data-toggle="modal" data-target="#splosnoStanovanje">
							<span class="fa fa-edit"></span> Uredi podatke
						</button>';
					}

					$result .= '
					<!-- Modal -->
					<div class="modal fade" id="splosnoStanovanje" role="dialog"  >
						<div class="modal-dialog modal-lg" >

							<!-- Modal content-->
							<div class="modal-content" >
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Spreminjanje podatkov o stanovanju</h4>
								</div>
								<div class="modal-body" >
									
									<form role="form" action="" method="post">	
<div class="text_stanovanje">
                            	Naslov
                        		</div>
                                	<br />	
                                <div class="form-group col-md-8 col-lg-8 col-sm-12">
			                    		<label class="sr-only" for="ulica">Ulica</label>
			                        	<input type="text" placeholder="Ulica" value="'.$this->getData('ulica').'" class="form-control" id="ulica" required>
			                        </div>
			                        <div class="form-group col-md-4 col-lg-4 col-sm-12">
			                        	<label class="sr-only" for="hisna_stevilka">Hišna Številka</label>
			                        	<input type="text" placeholder="Hišna številka" value="'.$this->getData('hisna_stevilka').'" class="form-control" id="hisna_stevilka" required>
			                        </div>
                                    
                                    
                                    <div class="form-group col-md-8 col-lg-8 col-sm-12">
			                    		<label class="sr-only" for="mesto">Mesto</label>
			                        	<input type="text" placeholder="Mesto" value="'.$this->getData('mesto').'" class="form-control" id="mesto" required>
			                        </div>
			                        <div class="form-group col-md-4 col-lg-4 col-sm-12">
			                        	<label class="sr-only" for="postna_stevilka">Poštna številka</label>
			                        	<input type="text" placeholder="Poštna številka" value="'.$this->getData('postna_stevilka').'" class="form-control" id="postna_stevilka" required>
			                        </div>';
                                    
                                     
						$selected_1_1 = "";
						$selected_1_2 = "";
						$selected_1_3 = "";
						$selected_1_4 = "";
						$selected_1_5 = "";
						switch($this->getData('stevilo_sob')) {
							case 1:
								$selected_1_1 = "selected";
							break;
							case 2:
								$selected_1_2 = "selected";
							break;
							case 3:
								$selected_1_3 = "selected";
							break;
							case 4:
								$selected_1_4 = "selected";
							break;
							case 5:
								$selected_1_5 = "selected";
							break;
						}
						$selected_2_1 = "";
						$selected_2_2 = "";
						$selected_2_3 = "";
						$selected_2_4 = "";
						$selected_2_5 = "";
						switch($this->getData('stevilo_postelj')) {
							case 1:
								$selected_2_1 = "selected";
							break;
							case 2:
								$selected_2_2 = "selected";
							break;
							case 3:
								$selected_2_3 = "selected";
							break;
							case 4:
								$selected_2_4 = "selected";
							break;
							case 5:
								$selected_2_5 = "selected";
							break;
						}
						$selected_3_1 = "";
						$selected_3_2 = "";
						$selected_3_3 = "";
						$selected_3_4 = "";
						$selected_3_5 = "";
						switch($this->getData('stevilo_prostih_mest')) {
							case 1:
								$selected_3_1 = "selected";
							break;
							case 2:
								$selected_3_2 = "selected";
							break;
							case 3:
								$selected_3_3 = "selected";
							break;
							case 4:
								$selected_3_4 = "selected";
							break;
							case 5:
								$selected_3_5 = "selected";
							break;
						}


                                $result .= ' 
                                <div class="text_stanovanje">
                            	Število sob / št. postelj na sobo / št. prostih mest
                        	</div>
                                 <br />
                                <div class="btn-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                <label class="sr-only" for="stevilo_sob">Število sob</label>
                                <select id="br_soba" class="form-control btn btn-default">
                                <option value="5" '.$selected_1_5.'>5-sobno</option>
                                <option value="4" '.$selected_1_4.'>4-sobno</option>
                                <option value="3" '.$selected_1_3.'>3-sobno</option>
  				<option value="2" '.$selected_1_2.'>2-sobno</option>
 				<option value="1" '.$selected_1_1.'>1-sobno</option>
 								
                                </select>
			                      
			                        </div>
                                  
                                <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                <label class="sr-only" for="stevilo_sob">Število postelj na sobo</label>
                                <select id="br_kreveta" class="form-control btn btn-default">
	                                <option value="5" '.$selected_2_5.'>5-posteljna</option>
	                                <option value="4" '.$selected_2_4.'>4-posteljna</option>
	                                <option value="3" '.$selected_2_3.'>3-posteljna</option>
	  				<option value="2" '.$selected_2_2.'>2-posteljna</option>
	 				<option value="1" '.$selected_2_1.'>1-posteljna</option>						
                                </select>
			                      
			                        </div>
                                    
                                     <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                <label class="sr-only" for="stevilo_sob">Število prostih mest</label>
                                <select id="br_mesta" class="form-control btn btn-default">
                                <option value="5" '.$selected_3_5.'>5-mest</option>
                                <option value="4" '.$selected_3_4.'>4-mesta</option>
                                <option value="3" '.$selected_3_3.'>3-mesta</option>
  				<option value="2" '.$selected_3_2.'>2-mesti</option>
 				<option value="1" '.$selected_3_1.'>1-mesto</option>
 								
                                </select>
			                      
			                        </div>
                                    
                                    </div>
                                     <br />
                                     <div class="text_stanovanje">
                            	Velikost stanovanja
                                </div>
                                     <br />
                                    <div class="input-group col-lg-10 col-md-12 col-sm-12 col-xs-12 col-lg-push-1">
  <input type="text" id="velikost_stanovanja" class="form-control" value="'.$this->getData('velikost_stanovanja').'" placeholder="Velikost stanovanja" aria-describedby="basic-addon2" required>
  <span class="input-group-addon" id="basic-addon2">m&sup2;</span>
</div>

                                     <br />
                                     <div class="text_stanovanje">
                            	Dodatki stanovanju (ustrezno označite)
                                </div>
                                 <br />
                                
                               
                                  <div class="btn-group">';
                                


				$stmt = $db->prepare("DESCRIBE kriterij");
				$stmt->execute();
				
				while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {

					$temp_str = explode("_", $row['Field']);
					if($temp_str[0] == "stan") {
						$str_main = str_replace("stan_", "", $row['Field']);
						$str_main_temp = str_replace("_", " ", $str_main);
						$str_main_temp = ucfirst($str_main_temp);

						$checked = "";
						if($this->getDataKriterij("stan_".$str_main) == 1) {
							$checked = "checked";
						}
						$result .= '<div class="form-group col-lg-4 col-md-6 col-xs-6">
								<div class="checkbox">
							           <label>
							             <input type="checkbox" id="'.$str_main.'" '.$checked.'>
							               '.$str_main_temp.'
								   </label>
								</div>
							    </div>';
					}
                                    
                                }
                                    
                                    
                                    
                                    
                                    
                               $result .= '</div>





                                     <br />
                                <div class="text_stanovanje">     
                            	Dodaten opis
                        	</div>
                                <br />
                                <div class="col-lg-12 col-md-12 col-xs-12" >
			                        	<label class="sr-only" for="opis">Opis</label>
			                        	<textarea type="text" id="opis_stanovanja" class="form-control" style="height:70px">'.$this->getData('opis').'</textarea>
			                        </div>
                                    </div>
<div class="modal-footer" style="margin-top:100px;">

              <button type="button" class="btn btn-primary" onclick="update_form(3)">

              <div id="form3_submit"><i class="fa fa-save"></i> Shrani</div>

              </button>

              <button type="button" class="btn btn-default" data-dismiss="modal">Zapri</button>

            </div>
							</div>

						</div>
					</div>

				</div>
				</form>


				</div>

			</div>


			
		</div>

	</div>';

	if($this->myStan == 1) {

	
	$result .= '
	<script>

		function update_form(form) {

			switch(form) {	
				case 1:

				break;
				case 2:	
					$("#form2_submit").html("<div style=\"width: 172px\"><i class=\"fa fa-spinner fa-pulse\"></i></div>");
					var stroski_najamnine = $("#stroski_najamnine").val();
					var stroski_obratovalni = $("#stroski_obratovalni").val();
					var stroski_vrsta = $("#stroski").val();
					var stroski_varscina = $("#stroski_varscina").val();
					update("'.encrypt("stanovanje_2").'", "'.encrypt("najemnina").'", stroski_najamnine, "'.encrypt($user->getID()).'");
					update("'.encrypt("stanovanje_2").'", "'.encrypt("obratovalni_stroski").'", stroski_obratovalni, "'.encrypt($user->getID()).'");
					update("'.encrypt("stanovanje_2").'", "'.encrypt("vrsta_stroskov").'", stroski_vrsta, "'.encrypt($user->getID()).'");
					var res = update("'.encrypt("stanovanje_2").'", "'.encrypt("varscina").'", stroski_varscina, "'.encrypt($user->getID()).'");
					if(res == 1) {	window.location.href = "'.$_baseURL.'profil_stanovanja.php?stan_id='.$this->getData('id').'";	}
				break;
				case 3:
					$("#form3_submit").html("<div style=\"width: 172px\"><i class=\"fa fa-spinner fa-pulse\"></i></div>");
					var ulica           	= $("#ulica").val();
					var hisna_stevilka  	= $("#hisna_stevilka").val();
					var mesto           	= $("#mesto").val();
					var postna_stevilka 	= $("#postna_stevilka").val();
					var br_soba         	= $("#br_soba").val();
					var br_kreveta      	= $("#br_kreveta").val();
					var br_mesta        	= $("#br_mesta").val();
					var velikost_stanovanja	= $("#velikost_stanovanja").val();
					var opis_stanovanja 	= $("#opis_stanovanja").val();';

				$stmt = $db->prepare("DESCRIBE kriterij");
				$stmt->execute();
				
				while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {

					$temp_str = explode("_", $row['Field']);
					if($temp_str[0] == "stan") {
						$str_main = str_replace("stan_", "", $row['Field']);
						$str_main_temp = str_replace("_", " ", $str_main);
						$str_main_temp = ucfirst($str_main_temp);

						$result .= 'var '.$str_main.' = 0;
							if($("#'.$str_main.'").is(":checked")) {	'.$str_main.' = 1;	} else {	'.$str_main.' = 0;	}
						';
						$result .= 'updateKriterij("'.encrypt("kriterij").'", "'.encrypt("stan_".$str_main).'", '.$str_main.', "'.encrypt($this->getData('id')).'");';
					}
                                    
                                }
					

				$result .= '
					update("'.encrypt("stanovanje_2").'", "'.encrypt("ulica").'", ulica, "'.encrypt($user->getID()).'");
					update("'.encrypt("stanovanje_2").'", "'.encrypt("hisna_stevilka").'", hisna_stevilka, "'.encrypt($user->getID()).'");
					update("'.encrypt("stanovanje_2").'", "'.encrypt("mesto").'", mesto, "'.encrypt($user->getID()).'");
					update("'.encrypt("stanovanje_2").'", "'.encrypt("postna_stevilka").'", postna_stevilka, "'.encrypt($user->getID()).'");
					update("'.encrypt("stanovanje_2").'", "'.encrypt("stevilo_sob").'", br_soba, "'.encrypt($user->getID()).'");
					update("'.encrypt("stanovanje_2").'", "'.encrypt("stevilo_postelj").'", br_kreveta, "'.encrypt($user->getID()).'");
					update("'.encrypt("stanovanje_2").'", "'.encrypt("stevilo_prostih_mest").'", br_mesta, "'.encrypt($user->getID()).'");
					update("'.encrypt("stanovanje_2").'", "'.encrypt("velikost_stanovanja").'", velikost_stanovanja, "'.encrypt($user->getID()).'");
					var res = update("'.encrypt("stanovanje_2").'", "'.encrypt("opis").'", opis_stanovanja, "'.encrypt($user->getID()).'");
					if(res == 1) {	window.location.href = "'.$_baseURL.'profil_stanovanja.php?stan_id='.$this->getData('id').'";	}
				break;
			}
			
		}
		function update(tab, column, data, user_id) {	
			var result = 0;

			var pod0 = 1;	
			var pod1 = tab;
			var pod2 = column;
			var pod3 = data;
			var pod4 = user_id;
			var pod5 = "'.encrypt("uporabnik_id").'";
			var pod6 = "'.encrypt($this->getData("id")).'";
			var pod7 = "'.encrypt("id").'";
			$.ajax({
				type: "POST",
				url: "'.$_baseURL.'ajax.php?lbrw='.encrypt($TOKEN).'",
				async: false,
				data: { zbrw: pod0, table: pod1, column: pod2, data: pod3, uid: pod4, uidc: pod5, stan_id: pod6, wCrit: pod7 },
				success: function(data) {
					//alert(data);   
					result = 1;
				},
				error: function(data) {
					console.log("Greska kod spremanja atributa. [" + column + "]");
				}
			});
			return result;
		}
		function updateKriterij(tab, column, data, user_id) {	
			var result = 0;

			var pod0 = 1;	
			var pod1 = tab;
			var pod2 = column;
			var pod3 = data;
			var pod4 = user_id;
			var pod5 = "'.encrypt("sOc_ID").'";
			var pod6 = "'.encrypt("stan").'";
			var pod7 = "'.encrypt("sOc").'";

			$.ajax({
				type: "POST",
				url: "'.$_baseURL.'ajax.php?lbrw='.encrypt($TOKEN).'",
				async: false,
				data: { zbrw: pod0, table: pod1, column: pod2, data: pod3, uid: pod4, uidc: pod5, stan_id: pod6, wCrit: pod7 },
				success: function(data) {
				//	alert(data);   
					result = 1;
				},
				error: function(data) {
					console.log("Greska kod spremanja atributa. [" + column + "]");
				}
			});
			return result;
		}

	</script>';
	}

	$result .= '

	<!-- USER PROFILE ROW END-->
	</div>
	<!-- CONATINER END -->';

		return $result;
		}
	}
?>
