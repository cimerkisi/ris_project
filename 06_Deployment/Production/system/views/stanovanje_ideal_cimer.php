<?php
class Stanovanje_ideal_cimer {

		var $id;
		var $data;
		var $myStan = 1;
		var $prijava = 0;
		
		function __construct($id) {
			$this->id = $id;
			$this->checkIfStanMy();
			$this->checkIfPrijavljen();
		}

		function checkIfStanMy() {
			global $user;
			global $db;

			$stmt = $db->prepare("SELECT * FROM stanovanje_2 WHERE id=? AND uporabnik_id=?");
			$stmt->execute(array($this->id, $user->getData('id_uporabnik')));
			$broji = $stmt->rowCount();
			if($broji == 0) {
				$this->myStan = 0;
			} else {
				$this->myStan = 1;
			}
		}

		function fetchData() {
			global $db;
			$stmt = $db->prepare("SELECT * FROM stanovanje_2 WHERE id=? LIMIT 1");
			$stmt->execute(array($this->id));
			$this->data = $stmt->fetch(PDO::FETCH_ASSOC);
		}

		function getData($name) {
			return $this->data[$name];
		}

		function checkIfPrijavljen() {
			global $user;
			global $db;

			$stmt = $db->prepare("SELECT * FROM prijavljeni WHERE stanovanje_id=? AND uporabnik_id=?");
			$stmt->execute(array($this->id, $user->getData('id_uporabnik')));
			$broji = $stmt->rowCount();
			if($broji == 0) {
				$this->prijava = 0;
			} else {
				$this->prijava = 1;
			}
		}

		function __toString() {
			global $_baseURL;
			global $TOKEN;
			global $user;
			global $db;

			$gsi1 = "edit";
			$gsi2 = "Uredi podatke o stanovanju";
			$gsi3 = "Prijavljene in potrjene osebe";
			if($this->myStan == 0) {
				$gsi1 = "info";
				$gsi2 = "Podatki o stanovanju";
				$gsi3 = "Cimri";
			}
			$result = '
<body class="">

	<!-- NAVBAR CODE END -->
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h2>Meni</h2>

				<br> <br>

			</div>
		</div>
		<!-- USER PROFILE ROW STARTS-->
		<div class="row">
        			<!-- PODMENI ZA POSTAVKE -->
			<div class="col-md-12 col-sm-6  user-wrapper">

				<ul class="nav nav-pills success">
                
                
					<li><a href="profil_stanovanja.php?stan_id='.$this->id.'"><i class="fa fa-'.$gsi1.'"></i> '.$gsi2.'</a></li>
                    <li><a href="stanovanje_slike.php?stan_id='.$this->id.'"><i class="fa fa-image"></i> Fotogalerija</a></li>
					<li class="active"><a href="stanovanje_ideal_cimer.php?stan_id='.$this->id.'"><i class="fa fa-users"></i>
							'.$gsi3.'</a></li>';
				if($this->myStan == 0) {
					if($this->prijava == 1) {
						$result .= '<li class="active" id="prijavaLI"><a href="javascript::void(0)" onclick="prijavaStan(0)" id="prijavaButton" style="background-color: green"><i class="fa fa-check"></i> Prijavljen</a></li>';
					} else {
						$result .= '<li class="active" id="prijavaLI"><a href="javascript::void(0)" onclick="prijavaStan(1)" id="prijavaButton" style="background-color: orange"><i class="fa fa-sign-in"></i> Prijavi se</a></li>';
					}

					$result .= '
						<script>
							function prijavaStan(status) {
								$("#prijavaButton").html("<div style=\"width: 85px; padding-left: 35px\"><i class=\"fa fa-spinner fa-pulse\"></i></div>");
								var data0 = 2;
								var data1 = "'.encrypt($this->getData('id')).'";
								var data2 = "'.encrypt($user->getData('id_uporabnik')).'";
								
								$.ajax({
									type: "POST",
									url: "'.$_baseURL.'ajax.php?lbrw='.encrypt($TOKEN).'",
									async: false,
									data: { zbrw: data0, kbrw: status, pod1: data1, pod2: data2 },
									success: function(data) {
										$("#prijavaLI").html(data);  
										result = 1;
									},
									error: function(data) {
										console.log("Greska");
									}
								});
							}
						</script>
					';
					
				}
$result .= '				</ul>


			</div>
  
			<div class="col-md-12 col-sm-12  user-wrapper" style="padding-top:20px">
				<div class="description">
					<i class="fa fa-group fa-2x"> Prijavljeni cimri</i>
					<hr>';

										
					$stmt = $db->prepare("SELECT * FROM prijavljeni WHERE stanovanje_id =?");
					$stmt->execute(array($this->id));

					while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
							$osoba = $db->prepare("SELECT * FROM uporabnik WHERE id_uporabnik=? LIMIT 1");
							$osoba->execute(array($row['uporabnik_id']));
							$osoba = $osoba->fetch(PDO::FETCH_ASSOC);
						$result .= '
						<div class="panel-group">
								<div class="panel panel-default">
									<div class="panel-heading" style="max-height:130px;min-height:50px;">
										<h4 class="panel-title">
											<a data-toggle="collapse" href="#stanovanje1" class="h3">'.$osoba['ime'].' '.$osoba['priimek'].'</a>
												<div class="btn-group" role="group" aria-label="..." style="float: right;">
	                                            
	                                            <a href="'.$_baseURL.'profil.php?user_id='.$osoba['id_uporabnik'].'" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="left" title="Upravljanje z stanovalci"><i class="fa fa-eye fa-2x"> </i></a>';
						if($this->myStan == 1) {
							if($row['status'] == "NOT_ACCEPTED_YET") {
								$result .= '<div id="insertCont" style="display:inline-block"><a href="javascript::void(0)" onclick="changeStat(1)" id="acceptButton" class="btn btn-xs btn-warning" data-toggle="tooltip" data-placement="left"><i class="fa fa-question-circle fa-2x"> </i></a></div>';
							} else if ($row['status'] == "ACCEPTED") {
			                                        $result .= '<div id="insertCont" style="display:inline-block"><a href="javascript::void(0)" onclick="changeStat(0)" id="acceptButton" class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="left"><i class="fa fa-check fa-2x"> </i></a></div>';
							}
							$result .= '
								<script>
									function changeStat(status) {
										$("#acceptButton").html("<div style=\"width: 20px; padding: 5px 6px 5px 5px\"><i class=\"fa fa-spinner fa-pulse\"></i>");
										var data0 = 3;
										var data1 = "'.encrypt($this->id).'";
										var data2 = "'.encrypt($osoba['id_uporabnik']).'";
										
										$.ajax({
											type: "POST",
											url: "'.$_baseURL.'ajax.php?lbrw='.encrypt($TOKEN).'",
											async: false,
											data: { zbrw: data0, kbrw: status, pod1: data1, pod2: data2 },
											success: function(data) {
												$("#insertCont").html(data);  
												result = 1;
											},
											error: function(data) {
												console.log("Greska");
											}
										});
									}
								</script>';
	       					} else {
							if($row['status'] == "ACCEPTED") {
			                                        $result .= '<div id="insertCont" style="display:inline-block"><a href="javascript::void(0)" style="cursor:default" class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="left"><i class="fa fa-check fa-2x"> </i></a></div>';
							} else {
								$result .= '<div id="insertCont" style="display:inline-block"><a href="javascript::void(0)" style="cursor:default" class="btn btn-xs btn-warning" data-toggle="tooltip" data-placement="left"><i class="fa fa-question-circle fa-2x"> </i></a></div>';
							}
						}
	                                           
						$result .= '					</div>
										</h4>
									</div>
									<div id="stanovanje1" class="panel-collapse collapse">
										<div class="panel-body">Informacije o stanovanju</div>
									</div>
								</div>
							</div>';
					}
						
$result .= '				</div>

			</div>


			
		</div>

	</div>


	<!-- USER PROFILE ROW END-->
	</div>
	<!-- CONATINER END -->



	<!-- REQUIRED SCRIPTS FILES -->
	<script data-rocketsrc="http://www.designbootstrap.com/track/ga.js"
		type="text/rocketscript" data-rocketoptimized="true"></script>
	<!-- CORE JQUERY FILE -->
	<script data-rocketsrc="assets/js/jquery-1.11.1.js"
		type="text/rocketscript" data-rocketoptimized="true"></script>
	<!-- REQUIRED BOOTSTRAP SCRIPTS -->
	<script data-rocketsrc="assets/js/bootstrap.js"
		type="text/rocketscript" data-rocketoptimized="true"></script>

</body>';

			return $result;
		}
	}
?>
