<?php

	class Head_noLogin {
		var $title = "TITLE";
		var $body = "<body>";

		var $username = "";
		var $password = "";

		function __construct($title) {
			$this->title = $title;
		}

		function setBodyID($id) {
			$this->body = '<body id="' . $id . '">';
		}

		function __toString() {
			global $_baseURL;

			$menu = new Menu_noLogin();
						
			return' 
			<!DOCTYPE html>
			<html lang="en">
			<head>
			    <meta charset="utf-8">
			    <meta http-equiv="X-UA-Compatible" content="IE=edge">
			    <meta name="viewport" content="width=device-width, initial-scale=1">
			    <meta name="description" content="">
			    <meta name="author" content="">
			    <link rel="icon" href="'.$_baseURL.'src/img/favicon.ico" type="image/x-icon"/>
			    <title>'.$this->title.'</title>
			    
			    <link rel="stylesheet" href="'.$_baseURL.'src/css_noLogin/bootstrap.css" type="text/css">
			    <link rel="stylesheet" href="'.$_baseURL.'src/assets_noLogin/css/form-elements.css">
				<link rel="stylesheet" href="'.$_baseURL.'src/assets_noLogin/css/style.css">

			    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
			    <link href="http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic" rel="stylesheet" type="text/css">
			    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
			    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
			    

			    
			    <link rel="stylesheet" href="'.$_baseURL.'src/css_noLogin/animate.min.css" type="text/css">
			    <link rel="stylesheet" href="'.$_baseURL.'src/css_noLogin/creative.css" type="text/css">
			    

			    
			   
			    
			   

			    
			    <!--[if lt IE 9]>
			        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
			    <![endif]-->
			</head>'
			. $this->body . $menu;
		}

	}

?>
