<?php

	class Menu {

		function __construct() {}
		
		function __toString() {
			global $_baseURL;
			return '
    			<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
		        <div class="container-fluid">
	                <div class="navbar-header">
		                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		                    <span class="sr-only">Toggle navigation</span>
	        	            <span class="icon-bar"></span>
	                	    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>
	        	        <a class="navbar-brand page-scroll" href="#page-top"><img src="'.$_baseURL.'/src/img/logo7.png" class="img-responsive"></a>
			</div>
		            <!-- Collect the nav links, forms, and other content for toggling -->
		            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		                <ul class="nav navbar-nav navbar-right">
						<li>
		                        <a class="page-scroll" href="index.php">Domov</a>
		                    </li>
		                    <li>
		                        <a href="login.php">Prijava</a>
		                    </li>
		                    <li>
		                        <a href="registracija.php">Registracija</a>
		                    </li>
		                </ul>
		            </div>
		            <!-- /.navbar-collapse -->
		        </div>
		        <!-- /.container-fluid -->
			</nav>';
		}

	}

?>
