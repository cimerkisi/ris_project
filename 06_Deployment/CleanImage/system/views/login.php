<?php

//	class Login extends MainView {
	class Login {

		function __construct() {}

		function __toString() {
			return '			
        <!-- Top content -->
		
		<header>
        <div class="header-content">
            <div class="header-content-inner">
		
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Prijava na spletno stran</h3>
                            	
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-key"></i>
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form role="form" action="" method="post" class="login-form">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">Uporabniško ime</label>
			                        	<input type="text" name="form-username" placeholder="Uporabniško ime..." class="form-username form-control" id="form-username">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Geslo</label>
			                        	<input type="password" name="form-password" placeholder="Geslo..." class="form-password form-control" id="form-password">
			                        </div>
			                        <button type="submit" class="btn btn-primary">Prijavi se</button>
			                    </form>
		                    </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
		</div>
		</div>
		</header>
			';
		}
	
	} 

?>
