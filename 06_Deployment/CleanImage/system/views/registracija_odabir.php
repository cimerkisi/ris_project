<?php

//	class Registration extends MainView {
	class Registracija_odabir {

		function __construct() {}

		function __toString() {
			return '			
        <!-- Top content -->
		 <header>
        <div class="header-content">
            <div class="header-content-inner">
		
        <div class="top-content" >
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box" style="margin-bottom:-40px;">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Registracija na spletno stran</h3>
                            	
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-pencil"></i>
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form role="form" action="dodajanje_stanovanja.php" method="post" class="login-form">			
									
									
			                        <button type="submit" class="btn btn-primary">Vnesi še ostale podatke </button>
                                    <small >(PODATKE IZPOLNI ZDAJ - o stanovanju...) </small>
									
			                    </form>
                                <br />
                                
                                <form role="form" action="login.php" method="post" class="login-form">			
									
									
			                        <button type="submit" class="btn btn-primary">Dokončaj registracijo </button>
                                    <small>(PODATKE IZPOLNI POZNEJE) </small>
									
			                    </form>
		                    </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        </div>
        </div>
       

		</header>
			';
		}
	
	}

?>
