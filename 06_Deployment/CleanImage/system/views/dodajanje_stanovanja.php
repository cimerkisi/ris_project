<?php

//	class Dodajanje_stanovanja extends MainView {
	class Dodajanje_stanovanja {

		function __construct() {}

		function __toString() {
			return '			
        <!-- Top content -->
		<header>
		 <div class="header-content">
            <div class="header-content-inner">
		
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                      <div class="col-sm-8 col-sm-offset-2 form-top form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Dodajanje stanovanja</h3>
                            	<h4>Naslov</h4>
                        		</div>
                        		<div class="form-top-right">
                                	<i class="fa fa-building-o"></i>
                        		</div>
                            </div>
                            <div class="form-bottom" >
			                    <form role="form" action="registracija3.html" method="post" class="login-form">	
                                		
                                <div class="form-group col-md-8 col-lg-8 col-sm-12">
			                    		<label class="sr-only" for="ulica">Ulica</label>
			                        	<input type="text" name="ulica" placeholder="Ulica" class="form-control" id="ulica">
			                        </div>
			                        <div class="form-group col-md-4 col-lg-4 col-sm-12">
			                        	<label class="sr-only" for="hisna_stevilka">Hišna Številka</label>
			                        	<input type="text" name="hisna_stevilka" placeholder="Hišna številka" class="form-control" id="hisna_stevilka">
			                        </div>
                                    
                                    
                                    <div class="form-group col-md-8 col-lg-8 col-sm-12">
			                    		<label class="sr-only" for="mesto">Mesto</label>
			                        	<input type="text" name="mesto" placeholder="Mesto" class="form-control" id="mesto">
			                        </div>
			                        <div class="form-group col-md-4 col-lg-4 col-sm-12">
			                        	<label class="sr-only" for="postna_stevilka">Poštna številka</label>
			                        	<input type="text" name="postna_stevilka" placeholder="Poštna številka" class="form-control" id="postna_stevilka">
			                        </div>
                                    
                                    
                                    
                                    
                                  <div class="form-top">
                                <div class="form-top-left">
                            	<h4>Število sob / št. postelj na sobo / št. prostih mest</h4>
                        		</div>
                                </div>
                                <div class="btn-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                <label class="sr-only" for="stevilo_sob">Število sob</label>
                                <select class="form-control btn btn-default">
                                <option value="4_sobno">5-sobno</option>
                                <option value="4_sobno">4-sobno</option>
                                <option value="3_sobno">3-sobno</option>
  								<option value="2_sobno">2-sobno</option>
 								<option value="1_sobno" selected>1-sobno</option>
 								
                                </select>
			                      
			                        </div>
                                  
                                <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                <label class="sr-only" for="stevilo_sob">Število postelj na sobo</label>
                                <select class="form-control btn btn-default">
                                <option value="mesano">mesano</option>
                                <option value="5_krevetna">5-krevetna</option>
                                <option value="4_krevetna">4-krevetna</option>
                                <option value="3_krevetna">3-krevetna</option>
  								<option value="2_krevetna">2-krevetna</option>
 								<option value="1_krevetna" selected>1-krevetna</option>
 								
                                </select>
			                      
			                        </div>
                                    
                                     <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                <label class="sr-only" for="stevilo_sob">Število prostih mest</label>
                                <select class="form-control btn btn-default">
                                <option value="5_mesta">5-mesta</option>
                                <option value="4_mesta">4-mesta</option>
                                <option value="3_mesta">3-mesta</option>
  								<option value="2_mesta">2-mesta</option>
 								<option value="1_mesto" selected>1-mesto</option>
 								
                                </select>
			                      
			                        </div>
                                    
                                    </div>
                                    
                                    <div class="form-top">
                                <div class="form-top-left">
                            	<h4>Velikost stanovanja</h4>
                        		</div>
                                </div>
                                    
                                    <div class="input-group col-lg-10 col-md-12 col-sm-12 col-xs-12 col-lg-push-1">
  <input type="text" name="velikost_stenovanja" class="form-control" placeholder="Velikost stanovanja" aria-describedby="basic-addon2">
  <span class="input-group-addon" id="basic-addon2">m&sup2;</span>
</div>

                                    
                                    <div class="form-top">
                                    <div class="form-top-left">
                            	<h4>Dodatki stanovanju (ustrezno označite)</h4>
                        		</div>
                                </div>
                                
                                <div class="btn-group">
                                
                                <div class="form-group col-lg-4 col-md-6 col-xs-6">
                                Internet:
			                        	<label class="sr-only"  for="internet">Internet</label>
			                        	<input type="checkbox" name="internet" class="form-control" id="internet">
			                        </div>
                                    
                                <div class="form-group col-lg-4 col-md-6 col-xs-6" >
                                Pralni stroj:
			                        	<label class="sr-only"  for="perilica">Perilica</label>
			                        	<input type="checkbox" name="perilica" class="form-control" id="perilica">
			                        </div>
                                    
                                     <div class="form-group col-lg-4 col-md-6 col-xs-6" >
                                Terasa:
			                        	<label class="sr-only"  for="terasa">Terasa</label>
			                        	<input type="checkbox" name="terasa" class="form-control" id="terasa">
			                        </div>
                                    
                                      <div class="form-group col-lg-4  col-md-6 col-xs-6">
                                Možnost parkiranja:
			                        	<label class="sr-only"  for="moznost_parkiranja">Moznost parkiranja</label>
			                        	<input type="checkbox" name="moznost_parkiranja" class="form-control" id="moznost_parkiranja">
			                        </div>
                                    
                                <div class="form-group col-lg-4  col-md-6 col-xs-6" >
                                Hišni ljubljenčki:
			                        	<label class="sr-only"  for="hisni_ljubljencki">Dovoljeni hišni ljubljenčki</label>
			                        	<input type="checkbox" name="hisni_ljubljencki" class="form-control" id="hisni_ljubljencki">
			                        </div>
                                    
                                     <div class="form-group col-lg-4 col-md-6 col-xs-6" >
                                Souporaba:
			                        	<label class="sr-only"  for="souporaba_s_studenti">Terasa</label>
			                        	<input type="checkbox" name="souporaba_s_studenti" class="form-control" id="souporaba_s_studenti">
			                        </div>
                                    
                                    <div class="form-group col-lg-4 col-md-6 col-xs-6" >
                                Opremljeno:
			                        	<label class="sr-only"  for="s_pohistvom">S pohistvom</label>
			                        	<input type="checkbox" name="s_pohistvom" class="form-control" id="s_pohistvom">
			                        </div>
                                    <div class="form-group col-lg-4 col-md-6 col-xs-6" >
                                Kadilci:
			                        	<label class="sr-only"  for="kadilci">Kadilci</label>
			                        	<input type="checkbox" name="kadilci" class="form-control" id="kadilci">
			                        </div>
                                    <div class="form-group col-lg-4 col-md-6 col-xs-6" >
                                Dvigalo:
			                        	<label class="sr-only"  for="dvigalo">Dvigalo</label>
			                        	<input type="checkbox" name="dvigalo" class="form-control" id="dvigalo">
			                        </div>
                                    
                                    <div class="form-group col-lg-4 col-md-6 col-xs-6" >
                                Center:
			                        	<label class="sr-only"  for="center">Center</label>
			                        	<input type="checkbox" name="center" class="form-control" id="center">
			                        </div>
                                    
                                    <div class="form-group col-lg-4 col-md-6 col-xs-6" >
                                Nakadilci:
			                        	<label class="sr-only"  for="nakadilci">Nakadilci</label>
			                        	<input type="checkbox" name="nekadilci" class="form-control" id="nekadilci">
			                        </div>
                                    
                                    </div>
                                    <div class="form-top">
                                    <div class="form-top-left">
                            	<h4>Dodaten opis</h4>
                        		</div>
                                </div>
                                    
                                <div class="col-lg-12 col-md-12 col-xs-12" >
			                        	<label class="sr-only" for="opis">Opis</label>
			                        	<input type="text" name="opis_stanovanja" placeholder="Opis" class="form-control" id="opis">
			                        </div>
                                    
                                  <div class="btn-group col-lg-12" style="margin-top:20px">			
			             <div class="col-md-6 col-lg-3 col-sm-6 col-xs-6 col-lg-push-7 col-md-push-12 col-sm-push-7 col-xs-push-4">
                          <button class=" btn btn-danger" type="submit" class="btn btn-primary">Preskoči </button> 
               </div>
               <div class="col-md-6 col-lg-3 col-sm-6 col-xs-6 col-lg-push-7 col-md-push-12 col-sm-push-6 col-xs-push-3" >  
               <button class="btn btn-primary" type="submit" class="btn btn-primary">Nadaljuj</button>
									</div>
                                    </div>
                                    
                                    </div>
			                    </form>
                    
		                    </div>
                  </div>
              </div>
          </div>
            </div>
            
        </div>
        </div>
        </div>
		</header>
			';
		}
	
	}

?>
