<?php

//	class HomePage extends MainView {
	class HomePage {

		function __construct() {}

		function __toString() {
			return '
    <header>
        <div class="header-content">
            <div class="header-content-inner">
                <h1>Rabiš cimra ali stanovanje?</h1>
                <hr>
                <div class="container-fluid" style="background-color:#333;opacity:0.8">
	<div class="row">
        <div class="container-fluid" >
            <form method="get" action="/" class="form-inline" style="float:none;margin-left:auto;margin-right:auto;">
                <input name="q" class="col-lg-9 col-md-9 col-xs-12" type="text"  placeholder="Napišite naziv mesta" style="padding:8px;margin:6px;margin-right:auto;margin-left:auto;color:#333;">
                
                <button type="submit" class="btn btn-primary col-lg-2 col-md-2 col-xs-12" style="margin-top:6px;margin-bottom:6px;margin-left:5px;font-size:20px;" > <span class="fa fa-search"></span> Najdi</button>
            </form>
        </div>
    </div>
</div>
      </div>
    </div>
  </div>
</div>
            </div>
        </div>
    </header>

    <section class="bg-primary">
        <div class="container">
            <div class="row" id="kontakt">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Kontaktirajte nas!</h2>
                    <hr class="primary">
                    <p>Ready to start your next project with us? That\'s great! Give us a call or send us an email and we will get back to you as soon as possible!</p>
                </div>
                <div class="col-lg-4 col-lg-offset-2 text-center">
                    <i class="fa fa-phone fa-3x wow bounceIn"></i>
                    <p>123-456-6789</p>
                </div>
                <div class="col-lg-4 text-center">
                    <i class="fa fa-envelope-o fa-3x wow bounceIn" data-wow-delay=".1s"></i>
                    <p><a href="mailto:cimerki-si@mail.com">cimerki-si@mail.com</a></p>
                </div>
            </div>
        </div>
    </section>
				
			';
		}
	
	} 

?>
