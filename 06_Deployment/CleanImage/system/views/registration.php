<?php

//	class Registration extends MainView {
	class Registration {

		function __construct() {}

		function __toString() {
			return '			
        <!-- Top content -->
		 <header>
        <div class="header-content">
            <div class="header-content-inner">
		
        <div class="top-content" >
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div  class="col-sm-8 col-sm-offset-2 form-top form-box" style="margin-bottom:-40px;">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Registracija na spletno stran</h3>
                            	
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-pencil"></i>
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form role="form" action="" method="post" class="login-form">
								
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-uporabnisko_ime">Ime</label>
			                        	<input type="text" name="ime" placeholder="Ime" class="form-control" id="uporabnisko_ime">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="priimek">Priimek</label>
			                        	<input type="text" name="priimek" placeholder="Priimek" class="form-control" id="priimek">
			                        </div>
									<div class="form-group">
			                        	<label class="sr-only" for="email">E-mail</label>
			                        	<input type="text" name="email" placeholder="E-mail" class="form-control" id="email">
			                        </div>
									<div class="form-group">
			                        	<label class="sr-only" for="geslo">Geslo</label>
			                        	<input type="password" name="geslo" placeholder="Geslo" class="form-control" id="geslo">
			                        </div>
									<div class="form-group">
			                        	<label class="sr-only" for="geslo">Potrditev gesla</label>
			                        	<input type="password" name="geslo_potrditev" placeholder="Potrditev gesla" class="form-control" id="geslo">
			                        </div>
			                        <button type="submit" class="btn btn-primary">Registriraj se</button>
									
			                    </form>
		                    </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
		</div>
		</div>

		</header>
			';
		}
	
	}

?>
