<?php

	class TheEnd {

		function __construct() {}

		function __toString() {
			global $_baseURL;

			echo '
			    <script src="'.$_baseURL.'src/assets/js/jquery-1.11.1.min.js"></script>
			    <script src="'.$_baseURL.'src/assets/js/jquery.backstretch.min.js"></script>
			    <script src="'.$_baseURL.'src/js/jquery.js"></script>
			    <script src="'.$_baseURL.'src/js/bootstrap.min.js"></script>
			    <script src="'.$_baseURL.'src/js/jquery.easing.min.js"></script>
			    <script src="'.$_baseURL.'src/js/jquery.fittext.js"></script>
			    <script src="'.$_baseURL.'src/js/wow.min.js"></script>
			    <script src="'.$_baseURL.'src/js/creative.js"></script>
			    <script src="'.$_baseURL.'src/assets/js/scripts.js"></script>
			</body>
			</html>
			';
		}

	}

?>
